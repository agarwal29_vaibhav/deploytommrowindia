import React, { useState, useEffect } from "react";
import Footer from "../Footer";
import Header from "../Header";
import "./ContactUs.css";
import heart from "../../Assets/redHeart.svg";

function ContactUs() {
  return (
    <React.Fragment>
      <Header />
      <div className="contactDiv">
        <p className="contactUsTittle">Contact Us</p>
        <p className="contactHelp">
          We`d &nbsp;
          <img src={heart} alt="heart" />
          &nbsp; to help you
        </p>
        <p className="contactTagline">
          We wish to create things with love, cheerful people. We love to hear
          from you.
        </p>
        <div className="ContactUsMainDiv row">
          <div className="col-6"></div>
          <div className="col-6"></div>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
}

export default ContactUs;
