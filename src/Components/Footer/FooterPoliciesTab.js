import React from "react";
import "./index.css";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import TermNConditions from "./Components/Terms&Condition";
import PrivacyPolicy from "./Components/PrivacyPolicy";
import CookiesPolicy from "./Components/CookiesPolicy";
import Header from "../Header";
import Footer from "../Footer";

function FooterPoliciesTab() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <p>{children}</p>
          </Box>
        )}
      </div>
    );
  }

  return (
    <div>
      <Header />
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          centered
          className="tabsDiv"
        >
          <Tab label="Terms & Condition" />
          <Tab label="Privacy Policy" />
          <Tab label="Cookies Policy" />
          {/*<Tab label="Lorem Ipsum" />
  <Tab label="Lorem Ipsum" />*/}
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <TermNConditions />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <PrivacyPolicy />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <CookiesPolicy />
      </TabPanel>
      <Footer />
    </div>
  );
}

export default FooterPoliciesTab;
