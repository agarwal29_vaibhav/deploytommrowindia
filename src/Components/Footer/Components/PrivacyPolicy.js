import React from "react";
import "../index.css";

function PrivacyPolicy() {
  return (
    <div>
      <p className="tnc_heading">Privacy Policy</p>
      <div className="tnc_mainDiv">
        <div>
          <p className="tnc_content">General</p>
          <div className="tnc_content">
            These Terms and Conditions (“T&C’s” or “Terms & Conditions” ) govern
            your access to and use of this website/(s)(“website” or “site” )
            operated by M/s. Tomorrows One Global Network Pvt. Ltd. (“TOGNPL” ).
            This statement discloses the privacy practices for the websites of
            TOGNPL, located at 3rd Floor, 306, Lusa Tower, Azadpur Commercial
            Complex, Azadpur, Delhi-110033.
          </div>
          <div className="tnc_content mt-2">
            As we update, improve and expand our Services, this policy may
            change, so please refer back to it periodically. By accessing the
            Company website or this Application or otherwise using the Services,
            you consent to collection, storage, and use of the personal
            information you provide (including any changes thereto as provided
            by you) for any of the services that we offer.
          </div>
          <p className="tnc_content">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero
          </p>
          <p className="tnc_content">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo
            <br />
            <br />
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua.
            <br />
            <br />
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum
          </p>
        </div>

        <div>
          <p className="tnc_sub-heading">What should user know at glance</p>
          <div>
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy eirmod tempor invidunt ut labore et dolore magna
              aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
              duo dolores et ea rebum. Stet clita kasd gubergren, no sea
              takimata sanctus est Lorem ipsum dolor sit
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy eirmod tempor invidunt ut labore et dolore magna
              aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
              duo dolores et ea rebum. Stet clita kasd gubergren, no sea
              takimata sanctus est Lorem ipsum dolor sit
            </div>
          </div>
        </div>
        <div>
          <p className="tnc_sub-heading">What should user know at glance</p>
          <div>
            <div className="tnc_content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero
            </div>
            <br />
            <div className="tnc_content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero
            </div>
          </div>
        </div>
        <div>
          <p className="tnc_sub-heading">Privacy of Use</p>
          <div>
            <div className="tnc_content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default PrivacyPolicy;
