import React from "react";
import "../index.css";

function CookiesPolicy() {
  return (
    <div>
      <p className="tnc_heading">Cookies Policy</p>
      <div className="tnc_mainDiv">
        <div>
          <p className="tnc_content">Lorem ipsum</p>
          <div className="tnc_content">
            • Lorem ipsum dolor sit amet, consetetur
          </div>
          <div className="tnc_content">
            • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy
          </div>
          <p className="tnc_content">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero
          </p>
          <p className="tnc_content">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo
            <br />
            <br />
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua.
            <br />
            <br />
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum
          </p>
        </div>

        <div>
          <p className="tnc_sub-heading">What should user know at glance</p>
          <div>
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy eirmod tempor invidunt ut labore et dolore magna
              aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
              duo dolores et ea rebum. Stet clita kasd gubergren, no sea
              takimata sanctus est Lorem ipsum dolor sit
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy eirmod tempor invidunt ut labore et dolore magna
              aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
              duo dolores et ea rebum. Stet clita kasd gubergren, no sea
              takimata sanctus est Lorem ipsum dolor sit
            </div>
          </div>
        </div>
        <div>
          <p className="tnc_sub-heading">What should user know at glance</p>
          <div>
            <div className="tnc_content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero
            </div>
            <br />
            <div className="tnc_content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero
            </div>
          </div>
        </div>
        <div>
          <p className="tnc_sub-heading">Privacy of Use</p>
          <div>
            <div className="tnc_content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
            <br />
            <div className="tnc_content">
              • Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
              diam nonumy
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CookiesPolicy;
