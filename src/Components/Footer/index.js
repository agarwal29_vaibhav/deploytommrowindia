import React from "react";
import InstaIcon from "../../Assets/instaIcon.svg";
import GoogleIcon from "../../Assets/googleIcon.svg";
import FacebookIcon from "../../Assets/fbIcon.svg";
import LinkedInIcon from "../../Assets/linkedinIcon.svg";
import TwitterIcon from "../../Assets/twitterIcon.svg";
import FooterIcon from "../../Assets/Logo-Motion.gif";
import "./index.css";

function Footer() {
  const footerHandler = () => {
    window.location.href = "/policies";
  };
  return (
    <footer className="footer">
      <div className="footerDiv1">
        <div style={{ flex: "3.5" }}>
          <p>Subscribe Our News letter</p>
          <input placeholder="Enter Your Mailing Address" />
        </div>
        {/*<div style={{ flex: "1" }}>
          <p>Become A Contributor</p>
          <button>Launch Your Product</button>
  </div>*/}
        <div style={{ position: "absolute", left: "72%" }}>
          <img src={FooterIcon} style={{ height: "10rem", width: "10rem" }} />
        </div>
      </div>
      <div className="footerDiv2">
        <div style={{ flex: "2.5" }} className="footerDiv2-list">
          <div>
            <p>COMPANY</p>
            <ul>
              <li>About Us</li>
              <li>Mission & Vision</li>
              <li>Contact Us</li>
            </ul>
          </div>
          <div>
            <p>PRODUCTS</p>
            <ul>
              <li>Business</li>
              <li>Culture</li>
              <li>Knowledge</li>
              {/*<li>Start Up</li>*/}
            </ul>
          </div>
          <div>
            <p>RESOURCES</p>
            <ul>
              <li>News</li>
              <li>Blogs</li>
              <li>FAQ</li>
              <li>Videos</li>
            </ul>
          </div>
        </div>
        <div style={{ flex: "1" }}></div>
        <div style={{ flex: "1", paddingTop: "8%" }} className="footerDiv2-sm">
          <p>Follow Us On Social Media</p>
          <ul>
            <li>
              <img src={TwitterIcon} className="footer-smIcons" />
            </li>
            <li>
              <img src={FacebookIcon} className="footer-smIcons" />
            </li>
            <li>
              <img src={LinkedInIcon} className="footer-smIcons" />
            </li>
            <li>
              <img src={InstaIcon} className="footer-smIcons" />
            </li>
            <li>
              <img src={GoogleIcon} className="footer-smIcons" />
            </li>
          </ul>
        </div>
      </div>
      <div className="footerDiv3">
        <ul>
          <li onClick={footerHandler}>Terms & Condition</li>
          <li onClick={footerHandler}>Privacy Policy</li>
          <li onClick={footerHandler}>Cookie Policy</li>
          {/*<li>Lorem Ipsum</li>
          <li>Lorem Ipsum</li>*/}
        </ul>
      </div>
    </footer>
  );
}

export default Footer;
