import React, { useState, useEffect } from "react";
import "./ProfileImg.css";
import profileImg from "../../Assets/GenderLeftLogo.svg";
import female from "../../Assets/Female.svg";
import male from "../../Assets/male.svg";
import male_selected from "../../Assets/male_selected.svg";
import female_selected from "../../Assets/female_selected.svg";
import transgender_selected from "../../Assets/transgender_selected.svg";
import Other from "../../Assets/Other.svg";

import { FormGroup, Label, Input } from "reactstrap";

function Gender() {
  const [age, setage] = useState("");
  const [selectInput, setselectInput] = useState("");
  const [gender, setGender] = useState(null);

  const ageHandler = (e) => {
    setage(e.target.value);
  };

  const dropdownHandler = (e) => {
    setselectInput(e.target.value);
  };
  const skipHandler = () => {
    window.location.href = "/";
  };

  const continueHandler = () => {
    window.location.href = "/";
  };
  return (
    <React.Fragment>
      <div className="row">
        <div className="ProfileLeftPannel col-6">
          <img src={profileImg} className="profileImg" />
        </div>
        <div className="ProfileRightPannel col-6">
          <div className="genderCard">
            <p className="identify">
              How do you identify yourself <br /> Rahul Kapoor
            </p>

            <div className="row" style={{ marginTop: "1.5rem" }}>
              <div className="col-4" onClick={() => setGender("M")}>
                {gender === "M" ? (
                  <img src={male_selected} className="genderSelectedImg" />
                ) : (
                  <div className="gen">
                    <img src={male} className="genderImg" />
                    <p className="genText">Male</p>
                  </div>
                )}
              </div>
              <div className="col-4" onClick={() => setGender("F")}>
                {gender === "F" ? (
                  <img src={female_selected} className="genderSelectedImg" />
                ) : (
                  <div className="gen">
                    <img src={female} className="genderImg" />
                    <p className="genText">Female</p>
                  </div>
                )}
              </div>
              <div className="col-4" onClick={() => setGender("O")}>
                {gender === "O" ? (
                  <img
                    src={transgender_selected}
                    className="genderSelectedImg"
                  />
                ) : (
                  <div className="gen">
                    <img src={Other} className="genderImg" />
                    <p className="genText">Other</p>
                  </div>
                )}
              </div>
            </div>

            <div className="row" style={{ marginTop: "1rem" }}>
              <div className="col-6">
                <p className="signUpLabel">Tell us your age</p>
                <Input
                  className="nameInput"
                  type="number"
                  style={{ marginTop: "0.5rem" }}
                  placeholder="Age"
                  value={age}
                  onChange={(e) => ageHandler(e)}
                />
              </div>
              <div className="col-6">
                <p className="signUpLabel">State</p>
                <div>
                  <Input
                    type="select"
                    name="select"
                    style={{
                      height: "2rem",
                      fontSize: "12px",
                      background: "#edf1f4 ",
                      outline: "none",
                      border: "none",
                      borderRadius:"8px",
                      marginTop: "0.5rem",
                    }}
                    value={selectInput}
                    onChange={dropdownHandler}
                  >
                    <option>Select Your State</option>
                    <option>Delhi</option>
                    <option>Mumbai</option>
                    <option>Kolkata</option>
                    <option>Chennai</option>
                  </Input>
                </div>
              </div>
            </div>

            <button
              className="genderContinueBtn"
              type="button"
              onClick={continueHandler}
            >
              Continue
            </button>
            <p className="skip" onClick={skipHandler}>
              Skip identity verification
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Gender;
