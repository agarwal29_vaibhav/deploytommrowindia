import React, { useState, useEffect } from "react";
import "./Login.css";
import loginImg from "../../Assets/loginIcon.svg";
import Header from "../Header";
import googleIcon from "../../Assets/google.svg";
import facebookIcon from "../../Assets/facebook.svg";
import twitterIcon from "../../Assets/twitter.svg";
import linkedinIcon from "../../Assets/linkedin-rect.svg";
import axios from "axios";

function Login() {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const userNameHandler = (e) => {
    setUserName(e.target.value);
  };

  const passwordHandler = (e) => {
    setPassword(e.target.value);
  };

  const forgetPasswordHandler = () => {};

  const signupHandler = () => {
    window.location.href = "/signup";
  };

  const LoginHandler = () => {
    axios
      .post("http://localhost:7000/CustomerLogin", {
        email: userName,
        password: password,
      })
      .then((res) => {
        console.log(res.data.data[0]);
        localStorage.setItem("CustomerLogin", JSON.stringify(res.data.data[0]));
      })
      .catch((err) => {
        console.log(err);
      });
    // window.location.href = "/";
  };

  return (
    <React.Fragment>
      <Header />
      <div className="row">
        <div className="LoginLeftPannel col-6">
          <img src={loginImg} className="loginImg" />
        </div>

        <div className="LoginRightPannel col-6">
          <p className="welcomeTitle">Welcome Back</p>
          <div className="loginCard">
            <div>
              <p className="loginLabel">Username or E-mail Address</p>
              <input
                className="loginInput"
                type="text"
                placeholder="Enter your Username"
                value={userName}
                onChange={(e) => userNameHandler(e)}
              />

              <p className="loginLabel marginTop1">Password</p>
              <input
                className="loginInput"
                type="password"
                placeholder="Enter your Password"
                value={password}
                onChange={(e) => passwordHandler(e)}
              />
            </div>

            <div className="row" style={{ marginTop: "1rem" }}>
              <div
                className="col-7"
                style={{ display: "flex", alignItems: "center" }}
              >
                <input
                  class="form-check-input rememberCheckbox"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label
                  class="form-check-label rememberPass"
                  for="flexCheckDefault"
                >
                  Remember me
                </label>
              </div>
              <div className="col-5">
                <p className="forgetPass" onClick={forgetPasswordHandler}>
                  Forgot Password
                </p>
              </div>
            </div>

            <button
              className="loginBtn"
              onClick={LoginHandler}
              style={{ marginBottom: "1rem" }}
            >
              Login
            </button>
            <p className="noAccount">
              Don't have an account ?{" "}
              <span className="loginSignUp" onClick={signupHandler}>
                Sign Up
              </span>
            </p>
            <p className="orSignUpWith">Or login with</p>
            <div className="row" style={{ margin: "2vw", marginTop: 0 }}>
              <div
                className="mediaIcon"
                style={{ background: "#ea4335 0% 0% no-repeat padding-box" }}
              >
                <img
                  src={googleIcon}
                  style={{ height: "30px", width: "30px" }}
                />
              </div>
              <div
                className="mediaIcon"
                style={{ background: "#4267B2 0% 0% no-repeat padding-box" }}
              >
                <img
                  src={facebookIcon}
                  style={{ height: "30px", width: "30px" }}
                />
              </div>
              <div
                className="mediaIcon"
                style={{ background: "#1DA1F2 0% 0% no-repeat padding-box" }}
              >
                <img
                  src={twitterIcon}
                  style={{ height: "30px", width: "30px" }}
                />
              </div>
              <div
                className="mediaIcon"
                style={{ background: "#0077B5 0% 0% no-repeat padding-box" }}
              >
                <img
                  src={linkedinIcon}
                  style={{ height: "30px", width: "30px" }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Login;
