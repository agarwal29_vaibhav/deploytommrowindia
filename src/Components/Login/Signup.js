import React, { useState, useEffect } from "react";
import "./Login.css";
import signupBackground from "../../Assets/signupBackground.png";
import mainLogo from "../../Assets/mainLogo.svg";
import tommrowIndia from "../../Assets/tommrowIndia.svg";
import googleIcon from "../../Assets/google.svg";
import facebookIcon from "../../Assets/facebook.svg";
import twitterIcon from "../../Assets/twitter.svg";
import linkedinIcon from "../../Assets/linkedin-rect.svg";
import India from "../../Assets/india.svg";

function Signup(props) {
  const [firstName, setfirstName] = useState("");
  const [lastName, setlastName] = useState("");
  const [mobNo, setmobNo] = useState("");
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");

  const userFirstNameHandler = (e) => {
    setfirstName(e.target.value);
  };
  const userLastNameHandler = (e) => {
    setlastName(e.target.value);
  };
  const mobNumberHandler = (e) => {
    setmobNo(e.target.value);
  };
  const emailHandler = (e) => {
    setemail(e.target.value);
  };
  const passwordHandler = (e) => {
    setpassword(e.target.value);
  };
  const confirmPasswordHandler = (e) => {
    setconfirmPassword(e.target.value);
  };

  const continueHandler = () => {
    window.location.href = "/otp-verification";
  };
  const loginHandler = () => {
    window.location.href = "/login";
  };

  return (
    <div
      style={{
        background: `url(${signupBackground})  no-repeat
        left center`,
        backgroundSize: "auto",
        height: "99.5vh",
        opacity: 1,
      }}
    >
      <div className="row signupLeftPannel">
        <img src={mainLogo} alt="logo" className="signupLogo" />
        <img src={tommrowIndia} alt="logo" className="signupName" />
      </div>

      <div className="signupCard">
        <p className="welcomeTitlesignup">Welcome To Tomorrow's India</p>
        <p className="exploreNew">Lets explore new things together</p>

        <div className="row">
          <div style={{ flex: "1", paddingLeft: "0" }}>
            <p className="signUpLabel">First Name</p>
            <input
              className="nameInput"
              type="text"
              placeholder="First Name"
              value={firstName}
              onChange={(e) => userFirstNameHandler(e)}
            />
          </div>
          <div style={{ flex: "1", paddingRight: "0" }}>
            <p className="signUpLabel">Last Name</p>
            <input
              className="nameInput"
              type="text"
              placeholder="Last Name"
              value={lastName}
              onChange={(e) => userLastNameHandler(e)}
            />
          </div>
        </div>
        <div className="row">
          <div className="mt-2 col-4">
            {/*style={{flex:"1", paddingLeft:"0"}}*/}
            <p className="signUpLabel">Country Code</p>
            <img
              src={India}
              style={{ marginLeft: ".5rem", marginRight: ".5rem" }}
            />
            <input
              className="FullInput"
              style={{ width: "3rem" }}
              type="number"
              value={+91}
            />
          </div>
          <div className="mt-2 col-8">
            {/*style={{flex:"3.5", paddingRight:"0"}}*/}
            <p className="signUpLabel">Mobile No.</p>
            <input
              className="FullInput"
              type="number"
              placeholder="Mobile No."
              min="10"
              max="10"
              value={mobNo}
              onChange={(e) => mobNumberHandler(e)}
            />
          </div>
        </div>

        <div className="mt-2">
          <p className="signUpLabel">E-mail ID</p>
          <input
            className="FullInput"
            type="email"
            placeholder="E-mail ID"
            value={email}
            onChange={(e) => emailHandler(e)}
          />
        </div>

        <div className="row mt-2">
          <div style={{ flex: "1", paddingLeft: "0" }}>
            <p className="signUpLabel">Password</p>
            <input
              className="nameInput"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => passwordHandler(e)}
            />
          </div>
          <div style={{ flex: "1", paddingRight: "0" }}>
            <p className="signUpLabel">Confirm Password</p>
            <input
              className="nameInput"
              type="password"
              placeholder="Confirm Password"
              value={confirmPassword}
              onChange={(e) => confirmPasswordHandler(e)}
            />
          </div>
        </div>

        <button className="signUpBtn mb-2" onClick={continueHandler}>
          Continue
        </button>
        <p className="noAccount">
          Already a member ?{" "}
          <span className="loginSignUp" onClick={loginHandler}>
            Log in
          </span>
        </p>
        <p className="orSignUpWith" style={{ margin: ".5rem 0 .5rem" }}>
          Or Sign Up with
        </p>
        <div className="row mt-3">
          <div
            className="mediaIcon"
            style={{
              background: "#ea4335 0% 0% no-repeat padding-box",
            }}
          >
            <img src={googleIcon} style={{ height: "30px", width: "30px" }} />
          </div>
          <div
            className="mediaIcon"
            style={{ background: "#4267B2 0% 0% no-repeat padding-box" }}
          >
            <img src={facebookIcon} style={{ height: "30px", width: "30px" }} />
          </div>
          <div
            className="mediaIcon"
            style={{ background: "#1DA1F2 0% 0% no-repeat padding-box" }}
          >
            <img src={twitterIcon} style={{ height: "30px", width: "30px" }} />
          </div>
          <div
            className="mediaIcon"
            style={{ background: "#0077B5 0% 0% no-repeat padding-box" }}
          >
            <img src={linkedinIcon} style={{ height: "30px", width: "30px" }} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Signup;
