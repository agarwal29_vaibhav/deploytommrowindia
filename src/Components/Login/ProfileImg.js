import React, { useState, useEffect } from "react";
import "./ProfileImg.css";
import profileImg from "../../Assets/profileImg.svg";
import CameraAltOutlinedIcon from "@mui/icons-material/CameraAltOutlined";
import user from "../../Assets/user.svg";

function ProfileImg() {
  const [img, setImg] = useState(null);
  const continueHandler = () => {
    window.location.href = "/gender";
  };

  const skipHandler = () => {
    window.location.href = "/";
  };
  const loadFile = (event) => {
    var output = document.getElementById("output");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
      URL.revokeObjectURL(output.src); // free memory
    };
  };

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImg(URL.createObjectURL(img));
    }
  };
  return (
    <React.Fragment>
      <div className="row">
        <div className="ProfileLeftPannel col-6">
          <img src={profileImg} className="profileImg" />
        </div>
        <div className="ProfileRightPannel col-6">
          <div className="profileImgCard">
            <div style={{ position: "relative" }}>
              {img ? (
                <div
                  style={{
                    height: "200px",
                    width: "200px",
                    borderRadius: "50%",
                    margin: "0 auto",
                  }}
                >
                  <img
                    src={img}
                    style={{
                      height: "100%",
                      width: "100%",
                      borderRadius: "50%",
                    }}
                  />
                </div>
              ) : (
                <div
                  style={{
                    height: "200px",
                    width: "200px",
                    borderRadius: "50%",
                    background: "#FF6A6AA9",
                    margin: "0 auto",
                  }}
                >
                  <img
                    src={user}
                    style={{
                      height: "100%",
                      width: "50%",
                    }}
                  />
                </div>
              )}
              <div
                style={{
                  background: "#ECECEC 0% 0% no-repeat padding-box",
                  border: "4px solid #FFFFFF",
                  borderRadius: "50%",
                  width: "2.5rem",
                  height: "2.5rem",
                  justifyContent: "center",
                  position: "absolute",
                  bottom: "0",
                  right: "35%",
                }}
              >
                <CameraAltOutlinedIcon
                  style={{ color: "darkgrey", marginTop: "0.125rem" }}
                />
              </div>
              <input
                className="upload-camera"
                type="file"
                name="myImage"
                onChange={(e) => onImageChange(e)}
              />
            </div>
            <p className="welcome">
              Welcome To Tomorrows India <br /> Rahul Kapoor
            </p>
            <p className="answerNext">
              Answers next few interesting questions, it will help us find right
              topics for you
            </p>
            <button
              className="profileContinueBtn"
              type="button"
              onClick={continueHandler}
            >
              Continue
            </button>
            <p className="skip" onClick={skipHandler}>
              Skip profile upload
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default ProfileImg;
