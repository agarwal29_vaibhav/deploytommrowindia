import React, { useState, useEffect } from "react";
import "./Login.css";
import "./Otp.css";
import signupBackground from "../../Assets/signupBackground.png";
import mainLogo from "../../Assets/mainLogo.svg";
import tommrowIndia from "../../Assets/tommrowIndia.svg";
import OtpInput from "react-otp-input";

function Otp() {
  const [code, setCode] = useState("");

  const handleChange = (code) => setCode(code);
  const resendHandler = () => {};
  const signUpHandler = () => {
    window.location.href = "/upload-image";
  };
  const skipHandler = () => {
    window.location.href = "/";
  };

  const footerHandler = () => {
    window.location.href = "/policies";
  };

  return (
    <div
      style={{
        background: `url(${signupBackground}) no-repeat left center`,
        backgroundSize: "auto",
        height: "100vh",
        opacity: 1,
      }}
    >
      <div className="row signupLeftPannel">
        <img src={mainLogo} alt="logo" className="signupLogo" />
        <img src={tommrowIndia} alt="logo" className="signupName" />
      </div>

      <div className="signupCard">
        <p className="welcomeTitlesignup">Welcome To Tomorrow's India</p>
        <p className="exploreNew">Lets explore new things together</p>

        <p className="verifyIdentity">Verify your identity</p>
        <p className="otpsent">
          OTP has been sent to your registered E-Mail ID
        </p>

        <OtpInput
          value={code}
          onChange={handleChange}
          numInputs={6}
          separator={<span style={{ width: "1rem" }}></span>}
          isInputNum={true}
          shouldAutoFocus={true}
          inputStyle={{
            width: "100%",
            height: "78px",
            fontSize: "20px",
            color: "#000",
            fontWeight: "400",
            caretColor: "blue",
            border: "3px solid #D1D1D1",
            borderRadius: "20px",
          }}
          focusStyle={{
            border: "3px solid #CFD3DB",
            outline: "none",
          }}
        />
        <p className="noOtp">
          Didn't receive OTP?
          <span className="resend" onClick={resendHandler}>
            Resend
          </span>
        </p>
        <div style={{ marginTop: "3rem" }}>
          <input
            class="form-check-input agreeCheckbox"
            type="checkbox"
            value=""
            id="flexCheckDefault"
          />
          <label class="form-check-label agreeLabel" for="flexCheckDefault">
            I agree to{" "}
            <span className="linkLabel" onClick={footerHandler}>
              Terms And Conditions
            </span>{" "}
            and{" "}
            <span className="linkLabel" onClick={footerHandler}>
              Privacy Policy
            </span>
            .
          </label>
        </div>

        <button className="loginBtn" onClick={signUpHandler}>
          Sign Up
        </button>

        <p className="skip" onClick={skipHandler}>
          Skip e-mail verification
        </p>
      </div>
    </div>
  );
}

export default Otp;
