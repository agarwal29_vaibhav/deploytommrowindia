import React from "react";
import mainLogo from "../../Assets/mainLogo.svg";
import SearchBox from "../../CommonUI/SearchComponent/Search Component";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import movieIcon from "../../Assets/movies.svg";
import seriesIcon from "../../Assets/webSeries.svg";
import "./index.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import Footer from "./Footer";
import Home from "../MainView/LandingPage";
import { Link } from "react-router-dom";

class Header extends React.Component {
  render() {
    const LoginHandler = () => {
      window.location.href = "/login";
    };

    const logoHandler = () => {
      window.location.href = "/";
    };

    const LogoutHandler = () => {
      localStorage.removeItem("CustomerLogin");
    };

    const MovieHandler = () => {
      window.location.href = "/all-movies-list";
    };
    const webSeriesHandler = () => {
      window.location.href = "/all-webseries-list";
    };
    return (
      <div className="header">
        <div className="logoDiv">
          <img src={mainLogo} className="mainLogo" onClick={logoHandler} />
          <p className="subLogoHeading">TOMORROW`S INDIA</p>
          <p className="subHeading">WE LAUNCH YOU DISCOVER</p>
        </div>

        <div className="navDiv">
          <p className="navHeading" id="businessNav">
            Business
          </p>
          <p className="navHeading">Knowledge</p>
          <p className="navHeading" id="cultureNav">
            Culture
            <div className="navHeaderDropdown">
              <ul>
                <li onClick={MovieHandler}>
                  <img src={movieIcon} className="header-navIcon" />
                  <div style={{ margin: "0 1.5vw" }}>
                    <p>Movies</p>
                    <span> Movies that move us</span>
                  </div>
                  <ArrowForwardIosIcon
                    style={{
                      color: "#0C2A4A",
                      width: "1rem",
                      height: "1rem",
                      marginTop: "0.25rem",
                      marginLeft: "auto",
                    }}
                  />
                </li>

                <li onClick={webSeriesHandler}>
                  <img src={seriesIcon} className="header-navIcon" />
                  <div style={{ margin: "0 1.5vw" }}>
                    <p>Web Series</p>
                    <span> Fascination of fantasy & reality</span>
                  </div>
                  <ArrowForwardIosIcon
                    style={{
                      color: "#0C2A4A",
                      width: "1rem",
                      height: "1rem",
                      marginTop: "0.25rem",
                      marginLeft: "auto",
                    }}
                  />
                </li>
              </ul>
            </div>
          </p>
          {/*<p className="navHeading">Start Ups</p>*/}
        </div>

        <div className="searchDiv">
          <SearchBox />
        </div>
        <div className="buttonDiv">
          <button className="buttonLaunch">Launch Your Product</button>
          <button className="buttonLogin" onClick={LoginHandler}>
            Login / Sign Up
          </button>
          {/*<button className="buttonLogin" onClick={LogoutHandler}>
            Logout
                  </button>*/}
        </div>
      </div>
    );
  }
}

export default Header;
