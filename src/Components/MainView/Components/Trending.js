import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../index.css";
import eyeIcon from "../../../Assets/eyeIcon.svg";
import heart from "../../../Assets/heart.svg";
import comment from "../../../Assets/comment.svg";
import shareIcon from "../../../Assets/shareIcon.svg";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";

import M83 from "../../../Assets/Movies/83.jpg";
import AtrangiRe from "../../../Assets/Movies/AtrangiRe.jpg";
import MinnalMurali from "../../../Assets/Movies/Minnal-Murali.jpg";
import LostInSpaceSeason3 from "../../../Assets/webSeries/Lost-In-Space-Season-3.png";
import Moneyheistseason5 from "../../../Assets/webSeries/Money-heist-season-5.jpg";

function Trending() {
  const [loadMore, setLoadmore] = useState(false);
  const list = [
    {
      fld_id: "1",
      fld_name: 83,
      fld_releaseplatform: "Netflix & Disney Plus Hotstar",
      fld_releasedate: 44554,
      fld_genre: "Drama,Sports",
      fld_language: "Hindi",
      fld_leadactors:
        "Ranveer Singh \r\nDeepika Padukone \r\nPankaj Tripathi \r\nTahir Raj Bhasin \r\nJiiva \r\nSaqib Saleem \r\nJatin Sarna \r\nChirag Patil \r\nDinker Sharma \r\nNishant Dahiya",
      fld_producers:
        "Deepika Padukone, Kabir Khan, Vishnuvardhan Induri, Sajid Nadiadwala, Madhu Mantena Varma",
      fld_productionhouse:
        "Reliance Entertainment ,Phantom Films, Vibri Media, KA Productions, Nadiadwala Grandson Entertainment,Kabir Khan Films",
      fld_plot:
        "Life of famous cricketer Kapil Dev, has been a historic event for India. His dedication towards his profession is tremendous over the years. Go and experience the prime time of Kapil’s Life.",
      fld_funfacts:
        "Ranveer Singh was learning the game from Dev before the start of shooting in June",
      fld_posterurl: "",
      fld_status: "Active",
      fld_updatedon: "24/12/2021 12:00 PM",
      fld_updatedby: 0,
      // fld_image: M83,
      fld_link: "https://www.youtube.com/embed/QHdkC6Kn0Io",
      fld_tittle: "Relive The 80s Through 83",
      fld_view: "481",
      fld_like: "366",
      fld_share: "34",
    },
    {
      fld_id: "4",
      fld_name: "Jersey",
      fld_releaseplatform: "Cinemas and Netflix",
      fld_releasedate:
        "31 December 2021-In cinemas\r\nFebruary 11, 2022-Netflix",
      fld_genre: "Drama,Sports",
      fld_language: "Hindi",
      fld_leadactors:
        "Shahid Kapoor \r\nMrunal Thakur \r\nRonit Kamra \r\nPankaj Kapur",
      fld_producers: "Dil Raju \r\nSuryadevara Naga Vamsi \r\nAman Gill",
      fld_productionhouse:
        "Allu Entertainment, Dil Raju Production, Sithara Entertainments and Brat Films",
      fld_plot:
        "What happens when a cricketer comes back to the field to fulfill his son’s desire for a jersey? The story of father-son relationship and determination gives an engaging sight to watch. Get ready to experience honest love on 31st of December.",
      fld_funfacts:
        "Shahid Kapoor received thirteen stitches on his lip during a scene, though he later recovered.\r\n-Production moved to Dehradun, and filming took place at the all-boys' boarding school, The Doon School, and Kasiga School.",
      fld_posterurl: "",
      fld_status: "Active",
      fld_updatedon: "24/12/2021 12:00 PM",
      fld_updatedby: 0,
      // fld_image: jersey,
      fld_link: "https://www.youtube.com/embed/BT0zd0kmTxM",
      fld_tittle: "A Dedicated Father",
      fld_view: "392",
      fld_like: "281",
      fld_share: "59",
    },
  ];

  const webList = [
    {
      fld_id: "12",
      fld_name: "Mismatched season 2",
      fld_releaseplatform: "Netflix",
      fld_releasedate: "To be announced",
      fld_genre: "Romantic comedy",
      fld_language: "Hindi",
      fld_seasonepisodes:
        "Seasons:2\r\nEpisodes:-\r\nSeason 1: 6\r\nSeason 2: 6",
      fld_actors:
        "-Prajakta Koli as Dimple Ahuja\r\n-Rohit Saraf as Rishi Singh Shekhawat\r\n-Rannvijay Singha as Professor Siddharth Sinha (Sid)\r\n-Vidya Malavade as Zeenat Karim\r\n-Suhasini Mulay as Rishi's Grandmother\r\n-Nidhi Singh as Warden",
      fld_producers: "Ronnie Screwvala",
      fld_productionhouse:
        "SVF Entertainment Pvt. Ltd.\r\nRSVP\r\nPratibha studio",
      fld_plotsumprevseason:
        "The romantic series features two people who are not right for each other. The story is about Dimple's character who is a tech wizard and Rishi who found Dimple's profile on a matrimonial website, and is now interested in pursuing her.",
      fld_currentsynopsis:
        "A heartbroken Rishi is now left with a tornado of emotions after seeing Dimple and Harsh’s closeness. While Dimple encounters Namrata’s sexuality which is also a realization for her about society.A complete drama package for a perfect binge watch! ",
      fld_prevrating:
        "Imdb- 5.4/10\r\nTimes of India- 3/5\r\nNews18- 2.5/5\r\nCommon sense media- 3/5",
      fld_mediapartners:
        "Wikipedia\r\nImdb\r\nFilmcompanion\r\nBizAsia\r\nYou Tube\r\nThe envoy web\r\nBinged\r\n\r\n\r\n",
      fld_funfacts:
        "-Mismatched is based on Sandhya Menon's 2017 novel When Dimple Met Rishi. \r\n-The story was adapted to take place in Jaipur for the web series, where the entire shooting took place.The book originally is set in United States",
      fld_status: "Active",
      fld_updatedby: "0",
      fld_updatedon: "18/12/2021 10:30 AM",
      // fld_image: MismatchedSeason2,
      fld_link: "https://www.youtube.com/embed/oorgtfS5MmQ",
      fld_tittle: "Will Rishi & Dimple Re-Unite?",
      fld_view: "430",
      fld_like: "320",
      fld_share: "67",
    },
    {
      fld_id: "2",
      fld_name: "Money Heist season 5",
      fld_releaseplatform: "Netflix",
      fld_releasedate: "3 December, 2021",
      fld_genre: "Action,Crime,Drama,Mystery,Thriller",
      fld_language: "English",
      fld_seasonepisodes:
        "Seasons:5(5th released in 2 parts)\r\nEpisodes:-\r\nPart 1: 13\r\nPart 2: 9\r\nPart 3: 8\r\nPart 4: 8\r\nPart 5:-(Further released in 2 parts)\r\nVolume 1: 5\r\nVolume 2: 5\r\n",
      fld_actors:
        "Enrique Arce as Arturo Roman\r\nMiguel Herran as Rio\r\nPedro Alonso as Berlin\r\nRodrigo de la Serna as Palermo\r\nItziar Ituno as Raquel Murillo\r\nJaime Lorente as Denver\r\nEsther Acebo as Monica Gaztambide\r\nBelen Cuesta as Manila\r\nDarko Peric as Helsinki\r\nNajwa Nimri as Alicia Sierra\r\nHovik Keuchkerian as Bogota\r\nLuka Peros as Marseille\r\nMario de la Rosa as Suarez\r\nFernando Cayo as Luis Tamayo\r\nFernando Soto as Angel Rubio\r\nJose Manuel Poga as Cesar Gandia",
      fld_producers: "Jesús Colmenar",
      fld_productionhouse: "Atresmedia, Pina-run Vancouver Media",
      fld_plotsumprevseason:
        "An unusual group of robbers attempt to carry out the most perfect robbery in Spanish history - stealing 2.4 billion euros from the Royal Mint of Spain.",
      fld_currentsynopsis:
        "The world of suspense has reached one notch above.Feed your curiosity and let all the questions be answered by Money Heist Part 5. This season will be nerve racking to watch. All the fans of Money Heist! Scratch the brand new episodes only on Netflix India.",
      fld_prevrating: "Imdb-8.3/10\r\nTimes Of India-3/5",
      fld_mediapartners:
        "Indulge Express\r\nNews 18\r\nYou Tube\r\nFacebook\r\nFox Exclusive\r\nKoi Moi\r\nPress Bolt News",
      fld_funfacts:
        '-Famous Brazilian football player, Neymar has a cameo appearance. He plays a monk.\r\n-The masks of the robbery are of the famous Spanish surrealist painter Salvador Dalí, who spoke highly of causing confusion and not limiting one\'s imagination - exactly what the Professor and the heist gang are doing.\r\n-The Bank of Spain did not allow the production crew to visit the real building to replicate it on set. The interior sets were mostly fictional. Acording to the team they used "a lot of gray and a dictatiorial flavour" to build the sets, inspired by the "Valley of the Fallen", a Francoist regime monument.\r\n-The outside of the building where the heist occurs isn\'t the actual Royal Mint of Spain but in fact the Spanish National Research Council in Madrid. There was an attempt to record it at the mint, which was denied by the institution. The interior scenes were recorded in a studio.',
      fld_status: "Active",
      fld_updatedby: "0",
      fld_updatedon: "18/12/2021 10:30 AM",
      fld_image: Moneyheistseason5,
      fld_link: "https://www.youtube.com/embed/uwsmkWh0S5Y",
      fld_tittle: "Witness the robbery turn into war: The Final Call",
      fld_view: "431",
      fld_like: "211",
      fld_share: "45",
    },
  ];
  const loadHandler = () => {
    setLoadmore(true);
  };
  return (
    <div className="topLaunch">
      <h1 className="trendingHeading">Trending</h1>
      <div>
        {list.map((val, index) => {
          return (
            <div className="trendingCard">
              <div className={"trendingCardImg" + (index + 1)}></div>
              <div className="trendingSubCard">
                <div className="trendingCardHeading">{val.fld_tittle}</div>
                <div className="launchCardContent">
                  {val.fld_plot &&
                    val.fld_plot.split(" ").splice(0, 45).join(" ")}
                </div>
                <div className="trendingSMDiv">
                  <div className="trendingExpress">
                    {val.fld_releaseplatform}
                  </div>
                  <div>
                    {/*<LocationOnIcon
                      style={{
                        width: "1rem",
                        height: "1rem",
                        marginTop: "-0.25rem",
                        marginLeft: "0.5vw",
                      }}
                    />*/}
                    <TwitterIcon
                      style={{
                        width: "1rem",
                        height: "1rem",
                        marginTop: "-0.25rem",
                        marginLeft: "0.5vw",
                      }}
                    />
                    <FacebookIcon
                      style={{
                        width: "1rem",
                        height: "1rem",
                        marginTop: "-0.25rem",
                        marginLeft: "0.5vw",
                      }}
                    />
                    <InstagramIcon
                      style={{
                        width: "1rem",
                        height: "1rem",
                        marginTop: "-0.25rem",
                        marginLeft: "0.5vw",
                      }}
                    />
                  </div>
                </div>
                <div className="trendingIconsDiv">
                  <span className="trendingI">
                    <img src={eyeIcon} className="trendingIcons" />
                    {val.fld_view}
                  </span>

                  <span className="trendingI">
                    <img src={heart} className="trendingIcons" /> {val.fld_like}
                  </span>
                  <span className="trendingI">
                    <img src={shareIcon} className="trendingIcons" />
                    {val.fld_share}
                  </span>
                </div>
                <div className="trendingFooter">
                  <button>
                    Share
                    <ShareOutlinedIcon
                      style={{
                        color: "#3C4852",
                        marginTop: "-4px",
                        width: "1rem",
                        height: "1rem",
                        marginLeft: "0.25vw",
                      }}
                    />
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div style={{ textAlign: "center" }}>
        {loadMore ? (
          webList.map((val, index) => {
            return (
              <div className="trendingCard">
                <div className={"trendingCardImg" + (index + 3)}></div>
                <div className="trendingSubCard">
                  <div className="trendingCardHeading">{val.fld_tittle}</div>
                  <div className="launchCardContent">
                    {val.fld_currentsynopsis}
                  </div>
                  <div className="trendingSMDiv">
                    <div className="trendingExpress">
                      {val.fld_releaseplatform}
                    </div>
                    <div>
                      {/*<LocationOnIcon
                        style={{
                          width: "1rem",
                          height: "1rem",
                          marginTop: "-0.25rem",
                          marginLeft: "0.5vw",
                        }}
                      />*/}
                      <TwitterIcon
                        style={{
                          width: "1rem",
                          height: "1rem",
                          marginTop: "-0.25rem",
                          marginLeft: "0.5vw",
                        }}
                      />
                      <FacebookIcon
                        style={{
                          width: "1rem",
                          height: "1rem",
                          marginTop: "-0.25rem",
                          marginLeft: "0.5vw",
                        }}
                      />
                      <InstagramIcon
                        style={{
                          width: "1rem",
                          height: "1rem",
                          marginTop: "-0.25rem",
                          marginLeft: "0.5vw",
                        }}
                      />
                    </div>
                  </div>
                  <div className="trendingIconsDiv">
                    <span className="trendingI">
                      <img src={eyeIcon} className="trendingIcons" />
                      {val.fld_view}
                    </span>

                    <span className="trendingI">
                      <img src={heart} className="trendingIcons" />
                      {val.fld_like}
                    </span>
                    <span className="trendingI">
                      <img src={shareIcon} className="trendingIcons" />
                      {val.fld_share}
                    </span>
                  </div>
                  <div className="trendingFooter">
                    <button>
                      Share
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                          marginLeft: "0.25vw",
                        }}
                      />
                    </button>
                  </div>
                </div>
              </div>
            );
          })
        ) : (
          <button
            className="readMoreBtn"
            onClick={loadHandler}
            value={loadMore}
          >
            Read More
          </button>
        )}
      </div>
    </div>
  );
}
export default Trending;
