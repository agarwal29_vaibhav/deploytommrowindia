import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../index.css";
import topLaunch1 from "../../../Assets/topLaunch-1.png";
import topLaunch2 from "../../../Assets/topLaunch-2.png";
import topLaunch3 from "../../../Assets/topLaunch-3.png";
import CardDivCompMovie from "./CardDivCompMovie";
import CardDivComp from "./CardDivComp";
import M83 from "../../../Assets/Movies/83.jpg";
import AtrangiRe from "../../../Assets/Movies/AtrangiRe.jpg";
import RRR from "../../../Assets/Movies/RRR.jpg";
import LostInSpaceSeason3 from "../../../Assets/webSeries/Lost-In-Space-Season-3.png";
import Moneyheistseason5 from "../../../Assets/webSeries/Money-heist-season-5.jpg";

function NewLaunchesComp() {
  const [loadMore, setLoadmore] = useState(false);
  const list = [
    {
      fld_id: "1",
      fld_name: 83,
      fld_releaseplatform: "Netflix & Disney Plus Hotstar",
      fld_releasedate: 44554,
      fld_genre: "Drama,Sports",
      fld_language: "Hindi",
      fld_leadactors:
        "Ranveer Singh \r\nDeepika Padukone \r\nPankaj Tripathi \r\nTahir Raj Bhasin \r\nJiiva \r\nSaqib Saleem \r\nJatin Sarna \r\nChirag Patil \r\nDinker Sharma \r\nNishant Dahiya",
      fld_producers:
        "Deepika Padukone, Kabir Khan, Vishnuvardhan Induri, Sajid Nadiadwala, Madhu Mantena Varma",
      fld_productionhouse:
        "Reliance Entertainment ,Phantom Films, Vibri Media, KA Productions, Nadiadwala Grandson Entertainment,Kabir Khan Films",
      fld_plot:
        "Life of famous cricketer Kapil Dev, has been a historic event for India. His dedication towards his profession is tremendous over the years. Go and experience the prime time of Kapil’s Life.",
      fld_funfacts:
        "Ranveer Singh was learning the game from Dev before the start of shooting in June",
      fld_posterurl: "",
      fld_status: "Active",
      fld_updatedon: "24/12/2021 12:00 PM",
      fld_updatedby: 0,
      fld_image: M83,

      fld_link: "https://www.youtube.com/embed/QHdkC6Kn0Io",
      fld_tittle: "Relive The 80s Through 83",
      fld_view: "323",
      fld_like: "261",
      fld_share: "11",
    },
    {
      fld_id: "2",
      fld_name: "Atrangi Re",
      fld_releaseplatform: "Disney + Hotstar",
      fld_releasedate: 44554,
      fld_genre: "Comedy, Drama, Romance",
      fld_language: "Hindi",
      fld_leadactors:
        "Dhanush,\r\nSara Ali Khan,\r\nAkshay Kumar, \r\n\r\nMohammed Zeeshan Ayyub.",
      fld_producers:
        "Bhushan Kumar \r\nKrishan Kumar \r\nAruna Bhatia \r\nHimanshu Sharma \r\nAanand L. Rai",
      fld_productionhouse:
        "T-Series \r\nColour Yellow Productions \r\nCape of Good Films",
      fld_plot:
        "A love triangle will be a fun-tickling 2021. This romantic drama is all set to woo its audience with the chirpy character of Sara Ali Khan and the mesmerized screen presence of Dhanush. While Sara plays a girl from Bihar, Dhanush will be seen as a guy from Madurai. \r\nThis kind of gives a vibe of the comeback of Dhanush in Bollywood. \r\n\r\n",
      fld_funfacts:
        "The cast includes two National Award winners: Dhanush and Akshay Kumar.\r\n",
      fld_posterurl: "",
      fld_status: "Active",
      fld_updatedon: "24/12/2021 12:00 PM",
      fld_updatedby: 0,
      fld_image: AtrangiRe,
      fld_link: "https://www.youtube.com/embed/wlLrCUVZ9Ko",
      fld_tittle: "Love Is A Confused Term",
      fld_view: "431",
      fld_like: "211",
      fld_share: "45",
    },
    {
      fld_id: "5",
      fld_name: "RRR",
      fld_releaseplatform: "In Cinemas",
      fld_releasedate: 44568,
      fld_genre: "Action, Drama",
      fld_language: "Telugu, Tamil, Kannada, Hindi",
      fld_leadactors:
        "Jr NTR, Ram Charan, Ajay Devgn , Alia Bhatt, Olivia Morris",
      fld_producers: "D. V. V. Danayya",
      fld_productionhouse: "DVV Entertainments",
      fld_plot:
        "This film is about two legendary freedom fighters, and the connected coincidence between them. Brace yourself to witness the grandeur journey of the actors fighting against the British Raj. \r\n",
      fld_funfacts:
        "Rajamouli scripted the film from the original story written by K. V. Vijayendra Prasad. Rajamouli came across reading stories about the incidents happened in the lives of Rama Raju and Bheem.",
      fld_posterurl: "",
      fld_status: "Active",
      fld_updatedon: "24/12/2021 12:00 PM",
      fld_updatedby: 0,
      fld_image: RRR,
      fld_link: "https://www.youtube.com/embed/f_vbAtFSEc0",
      fld_tittle: "The Heroism in The Blood",
      fld_view: "475",
      fld_like: "264",
      fld_share: "67",
    },
  ];

  const webList = [
    {
      fld_id: "1",
      fld_name: "Lost in Space 3",
      fld_releaseplatform: "Netflix",
      fld_releasedate: "1 December,2021",
      fld_genre: "Action,Sci-fi",
      fld_language: "English",
      fld_seasonepisodes:
        "Seasons:3\r\nEpisodes:-\r\nSeason 1:10\r\nSeason 2:10\r\nSeason 3: 8",
      fld_actors:
        "Molly Parker as Maureen Robinson \n Toby Stephens as John Robinson\r\nMaxwell Jenkins as Will Robinson\r\nTaylor Russell as Judy Robinson\r\nMina Sundwall as Penny Robinson\r\nIgnacio Serricchio as Don West\r\nParker Posey as June Harris / Dr. Smith\r\nBrian Steele as the Robot",
      fld_producers: "Kevin Burns and Jon Janashi",
      fld_productionhouse:
        "Legendary Television, Synthesis Entertainment, Clickety-Clack Productions, and Applebox Entertainment",
      fld_plotsumprevseason:
        "Lost in Space, a reconceptualized version of the 1965 American science fiction series is based on the adventures of a family of space colonists whose spaceship veers off course.",
      fld_currentsynopsis:
        "In the third and final season of Lost in Space, The world of earth isn’t satisfying for the Robinson Family. The stakes are higher than ever and it is to be seen how the Robinson family live up the conditions in space. Get along with the Robinson family and see how their survival instincts will be put to the ultimate test. ",
      fld_prevrating: "7.5/10\r\n",
      fld_mediapartners:
        "The Teal Mango\r\nSpace.com\r\nDigital Spy\r\nOracle Globe\r\nThe Live Mirror\r\n",
      fld_funfacts:
        "The crew shot very little of Lost in Space on sets. The alien world the Robinsons are stranded on is an actual, dressed location instead of a bunch of sound stages papered with green screens.",
      fld_status: "Active",
      fld_updatedby: "0",
      fld_updatedon: "18/12/2021 10:30 AM",
      fld_image: LostInSpaceSeason3,
      fld_link: "https://www.youtube.com/embed/TiCxFQYpktU",
      fld_tittle: "How About Living In Space?",
      fld_view: "581",
      fld_like: "361",
      fld_share: "145",
    },
    {
      fld_id: "2",
      fld_name: "Money Heist season 5",
      fld_releaseplatform: "Netflix",
      fld_releasedate: "3 December, 2021",
      fld_genre: "Action,Crime,Drama,Mystery,Thriller",
      fld_language: "English",
      fld_seasonepisodes:
        "Seasons:5(5th released in 2 parts)\r\nEpisodes:-\r\nPart 1: 13\r\nPart 2: 9\r\nPart 3: 8\r\nPart 4: 8\r\nPart 5:-(Further released in 2 parts)\r\nVolume 1: 5\r\nVolume 2: 5\r\n",
      fld_actors:
        "Enrique Arce as Arturo Roman\r\nMiguel Herran as Rio\r\nPedro Alonso as Berlin\r\nRodrigo de la Serna as Palermo\r\nItziar Ituno as Raquel Murillo\r\nJaime Lorente as Denver\r\nEsther Acebo as Monica Gaztambide\r\nBelen Cuesta as Manila\r\nDarko Peric as Helsinki\r\nNajwa Nimri as Alicia Sierra\r\nHovik Keuchkerian as Bogota\r\nLuka Peros as Marseille\r\nMario de la Rosa as Suarez\r\nFernando Cayo as Luis Tamayo\r\nFernando Soto as Angel Rubio\r\nJose Manuel Poga as Cesar Gandia",
      fld_producers: "Jesús Colmenar",
      fld_productionhouse: "Atresmedia, Pina-run Vancouver Media",
      fld_plotsumprevseason:
        "An unusual group of robbers attempt to carry out the most perfect robbery in Spanish history - stealing 2.4 billion euros from the Royal Mint of Spain.",
      fld_currentsynopsis:
        "The world of suspense has reached one notch above.Feed your curiosity and let all the questions be answered by Money Heist Part 5. This season will be nerve racking to watch. All the fans of Money Heist! Scratch the brand new episodes only on Netflix India.",
      fld_prevrating: "Imdb-8.3/10\r\nTimes Of India-3/5",
      fld_mediapartners:
        "Indulge Express\r\nNews 18\r\nYou Tube\r\nFacebook\r\nFox Exclusive\r\nKoi Moi\r\nPress Bolt News",
      fld_funfacts:
        '-Famous Brazilian football player, Neymar has a cameo appearance. He plays a monk.\r\n-The masks of the robbery are of the famous Spanish surrealist painter Salvador Dalí, who spoke highly of causing confusion and not limiting one\'s imagination - exactly what the Professor and the heist gang are doing.\r\n-The Bank of Spain did not allow the production crew to visit the real building to replicate it on set. The interior sets were mostly fictional. Acording to the team they used "a lot of gray and a dictatiorial flavour" to build the sets, inspired by the "Valley of the Fallen", a Francoist regime monument.\r\n-The outside of the building where the heist occurs isn\'t the actual Royal Mint of Spain but in fact the Spanish National Research Council in Madrid. There was an attempt to record it at the mint, which was denied by the institution. The interior scenes were recorded in a studio.',
      fld_status: "Active",
      fld_updatedby: "0",
      fld_updatedon: "18/12/2021 10:30 AM",
      fld_image: Moneyheistseason5,
      fld_link: "https://www.youtube.com/embed/uwsmkWh0S5Y",
      fld_tittle: "Witness the robbery turn into war: The Final Call",
      fld_view: "831",
      fld_like: "511",
      fld_share: "245",
    },
  ];
  const loadHandler = () => {
    setLoadmore(true);
  };
  return (
    <div className="topLaunch">
      <h1 className="topLaunchHeading">Top Launches</h1>
      <div className="topLaunchSubHeading">
        Know what's creating a Buzzzz...
      </div>
      <div>
        {list.map((val) => {
          return (
            <CardDivCompMovie
              imgSrc={val.fld_image}
              divClass="topLaunchCard"
              data={val.fld_plot}
              allData={val}
              hideSubContent={true}
            />
          );
        })}
      </div>
      <div>
        {loadMore ? (
          webList.map((val) => {
            return (
              <CardDivComp
                imgSrc={val.fld_image}
                divClass="topLaunchCard"
                hideSubContent={true}
                data={val.fld_plotsumprevseason}
                allData={val}
              />
            );
          })
        ) : (
          <div style={{ textAlign: "center" }}>
            <button
              className="loadMoreBtn"
              onClick={loadHandler}
              value={loadMore}
            >
              Load More
            </button>
          </div>
        )}
      </div>
    </div>
  );
}
export default NewLaunchesComp;
