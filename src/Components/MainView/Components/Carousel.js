import React from "react";
import Carousel from "react-bootstrap/Carousel";
import "bootstrap/dist/css/bootstrap.min.css";
import carouselImg1 from "../../../Assets/carousal_img1.png";
import carouselImg2 from "../../../Assets/carousal_img2.png";
import carouselImg3 from "../../../Assets/carousal_img3.png";
import carouselVideo from "../../../Assets/crauselVideo.mp4";
import "../index.css";
import nextIcon from "../../../Assets/nextIcon.svg";
import prevIcon from "../../../Assets/prevIcon.svg";

function CarouselComponent() {
  return (
    <Carousel
      nextIcon={<img src={nextIcon} className="nextPrevIcon" />}
      prevIcon={<img src={prevIcon} className="nextPrevIcon" />}
    >
      <Carousel.Item>
        <img className="d-block w-100" src={carouselImg1} alt="First slide" />
        {/*<Carousel.Caption
          style={{
            width: "46vw",
            left: "28%",
            right: "28%",
          }}
        >
          <h1>Joy Is A Net Of Love By Which You Can Catch Souls</h1>
        </Carousel.Caption>*/}
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={carouselImg2} alt="Second slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={carouselImg3} alt="Third slide" />
      </Carousel.Item>
      {/*<Carousel.Item>
        <video width="900" height="550" controls>
          <source src={carouselVideo} type="video/mp4" />
          Your browser does not support the video tag.
      </video>
      </Carousel.Item>*/}
    </Carousel>
  );
}
export default CarouselComponent;
