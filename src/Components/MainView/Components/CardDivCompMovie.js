import React from "react";
import "../index.css";
import eyeIcon from "../../../Assets/eyeIcon.svg";
import heart from "../../../Assets/heart.svg";
import comment from "../../../Assets/comment.svg";
import shareIcon from "../../../Assets/shareIcon.svg";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import LaunchLogo from "../../../Assets/LaunchLogo.svg";
import WebSeries from "../WebSeriesPage";
import { Link } from "react-router-dom";

function CardDivComp(props) {
  const allData = props.allData;
  console.log(allData, "qqqqqqq");
  return (
    <div className={props.divClass}>
      <img src={props.imgSrc} className="launchCardImg" />
      <div className="topLaunchSubCard">
        <div className="topLaunchMainCard">
          <div className="blnkDiv"></div>
          <span className="blankCircle">
            <img src={LaunchLogo} className="launchIcon" />
          </span>
          <div className="launchCardIconsDiv">
            <div style={{ display: "flex", alignItems: "center" }}>
              <span className="launchCardI">
                <img src={eyeIcon} className="launchCardIcons" />
                {allData.fld_view}
              </span>
              {/*<span className="launchCardI">
                <img src={comment} className="launchCardIcons" /> 200
  </span>*/}
              <span className="launchCardI">
                <img src={heart} className="launchCardIcons" />{" "}
                {allData.fld_like}
              </span>
              <span className="launchCardI">
                <img src={shareIcon} className="launchCardIcons" />
                {allData.fld_share}
              </span>
            </div>
            <div>
              <span className="iconBg">
                <BookmarkBorderIcon
                  style={{ color: "#3C4852", marginTop: "-4px" }}
                />
              </span>
              <span className="iconBg">
                <ShareOutlinedIcon
                  style={{ color: "#3C4852", marginTop: "-4px" }}
                />
              </span>
            </div>
          </div>
          <p className="launchCardHeading">{allData.fld_tittle}</p>
          <div
            className="launchCardContent"
            style={false ? { display: "none" } : {}}
          >
            {allData.fld_plot &&
              allData.fld_plot.split(" ").splice(0, 28).join(" ")}
            {"...."}
          </div>
          <div className="launchCardFooter">
            <Link
              to={{
                pathname: "/movie",
                state: allData,
              }}
            >
              <button>Discover Now</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardDivComp;
