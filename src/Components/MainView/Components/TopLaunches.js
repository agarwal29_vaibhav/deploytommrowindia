import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../index.css";
import newLaunch1 from "../../../Assets/newLaunch1.png";
import newLaunch2 from "../../../Assets/newLaunch2.png";
import newLaunch3 from "../../../Assets/newLaunch3.png";
import newLaunch4 from "../../../Assets/newLaunch4.png";
import newLaunch5 from "../../../Assets/newLaunch5.png";
import newLaunch6 from "../../../Assets/newLaunch6.png";
import newLaunch7 from "../../../Assets/newLaunch7.png";
import newLaunch8 from "../../../Assets/newLaunch8.png";
import eyeIcon from "../../../Assets/eyeIcon.svg";
import heart from "../../../Assets/heart.svg";
import comment from "../../../Assets/comment.svg";
import shareIcon from "../../../Assets/shareIcon.svg";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import CardDivComp from "./CardDivComp";

function TopLaunches() {
  return (
    <div className="newLaunch">
      <h1 className="newLaunchHeading">New Launches Near you</h1>
      <div className="newLaunchLine"></div>
      <div className="newLaunchCardDiv">
        <CardDivComp imgSrc={newLaunch1} divClass="newLaunchCard" />
        <div className="newLaunchList">
          <ul>
            <li>
              <img src={newLaunch2} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src={newLaunch3} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src={newLaunch4} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src={newLaunch5} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src={newLaunch6} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src={newLaunch7} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src={newLaunch8} className="newLaunchListImg" />
              <div>
                <p className="newLaunchListContent">
                  But I must explain to you how all this mistaken idea of
                  denouncing pleasure and praising pain was born and I
                </p>
                <div className="newLaunchCardIconsDiv">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <span className="newLaunchCardI">
                      <img src={eyeIcon} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={comment} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={heart} className="newLaunchCardIcons" /> 200
                    </span>
                    <span className="newLaunchCardI">
                      <img src={shareIcon} className="newLaunchCardIcons" /> 200
                    </span>
                  </div>
                  <div>
                    <span className="newLIconBg">
                      <BookmarkBorderIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                    <span className="newLIconBg">
                      <ShareOutlinedIcon
                        style={{
                          color: "#3C4852",
                          marginTop: "-4px",
                          width: "1rem",
                          height: "1rem",
                        }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
export default TopLaunches;
