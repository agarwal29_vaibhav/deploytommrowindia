import React from "react";
import Footer from "../Footer";
import Header from "../Header";
import CarouselComponent from "./Components/Carousel";
import NewLaunchesComp from "./Components/NewLaunches";
import TopLaunches from "./Components/TopLaunches";
import Trending from "./Components/Trending";
import "./index.css";

function LandingPage() {
  return (
    <div>
      <Header />
      <CarouselComponent />
      <NewLaunchesComp />
      <Trending />
      {/*<TopLaunches />*/}
      <Footer />
    </div>
  );
}
export default LandingPage;
