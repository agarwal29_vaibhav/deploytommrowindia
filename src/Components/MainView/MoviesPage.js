import React from "react";
import movie1 from "../../Assets/movie1.jpg";
import StarIcon from "@mui/icons-material/Star";
import StarHalfIcon from "@mui/icons-material/StarHalf";
import AmazonPrime from "../../Assets/AmazonPrime.svg";
import Netflix from "../../Assets/Netflix.svg";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Typography from "@mui/material/Typography";
import HomeIcon from "@mui/icons-material/Home";
import Link from "@mui/material/Link";

function Movies(props) {
  const handleClick = (event) => {
    event.preventDefault();
    console.info("You clicked a breadcrumb.");
  };
  const handleClickHome = () => {
    window.location.href = "/";
  };
  const allData = props.location.state;

  console.log(allData, "movies");
  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      href="/"
      className="breadcrumb"
      onClick={handleClickHome}
    >
      <HomeIcon style={{ width: "1rem", height: "1rem", color: "#314351" }} />
      Home
    </Link>,
    <Link
      underline="hover"
      key="2"
      color="inherit"
      className="breadcrumb"
      href="/getting-started/installation/"
      onClick={handleClick}
    >
      Movies
    </Link>,
    <Typography key="3" color="text.primary" className="breadcrumb">
      {allData.fld_name}
    </Typography>,
  ];

  return (
    <React.Fragment>
      <div>
        <iframe
          width="100%"
          height="480"
          src={allData.fld_link}
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
      </div>
      <div className="moviesDiv">
        <Breadcrumbs separator="›" aria-label="breadcrumb">
          {breadcrumbs}
        </Breadcrumbs>
        <div className="movieInfoDiv">
          <div className="movieLogoDiv">
            <img src={allData.fld_image} style={{ width: "100%" }} />
          </div>
          <div className="movieContentDiv">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <h1 className="movieName">{allData.fld_name}</h1>
              <div className="movieRatingDiv">
                <StarIcon style={{ color: "#FFE120" }} />
                <StarIcon style={{ color: "#FFE120" }} />
                <StarIcon style={{ color: "#FFE120" }} />
                <StarIcon style={{ color: "#FFE120" }} />
                <StarHalfIcon style={{ color: "#FFE120" }} />
              </div>
            </div>
            <div style={{ marginBottom: "1rem", marginTop: "0.5rem" }}>
              {allData.fld_releaseplatform.includes("Netflix") ? (
                <img
                  src={Netflix}
                  className="socialMediaLogo"
                  style={{ marginRight: "1vw" }}
                />
              ) : null}
              {allData.fld_releaseplatform.includes("Amazon Prime") ? (
                <img src={AmazonPrime} className="socialMediaLogo" />
              ) : null}
            </div>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Genre:</span>
              <span className="categoryDesc">{allData.fld_genre}</span>
            </p>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Language:</span>
              <span className="categoryDesc">{allData.fld_language}</span>
            </p>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Release Date:</span>
              <span className="categoryDesc">{allData.fld_releasedate}</span>
            </p>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Platform:</span>
              <span className="categoryDesc">
                {allData.fld_releaseplatform}
              </span>
            </p>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Producer</span>
              <span className="categoryDesc">{allData.fld_producers}</span>
            </p>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Production House</span>
              <span className="categoryDesc">
                {allData.fld_productionhouse}
              </span>
            </p>
            <p style={{ display: "flex", marginBottom: "0.5rem" }}>
              <span className="categoryName">Cast:</span>
              <span className="categoryDesc">{allData.fld_leadactors}</span>
            </p>
          </div>
        </div>
        <div className="plotFactDiv">
          <div className="plotFactHeadDiv">Plot</div>
          <div className="plotFactContentDiv">{allData.fld_plot}</div>
        </div>
        <div className="plotFactDiv">
          <div className="plotFactHeadDiv">Fun Fact</div>
          <div className="plotFactContentDiv">{allData.fld_funfacts}</div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default Movies;
