import React from "react";
import CardDivComp from "./Components/CardDivComp";
import listing1 from "../../Assets/listing1.png";
import listing2 from "../../Assets/listing2.png";
import listing3 from "../../Assets/listing3.png";
import listing4 from "../../Assets/listing4.png";
import listing5 from "../../Assets/listing5.png";
import listing6 from "../../Assets/listing6.png";
import listing7 from "../../Assets/listing7.png";
import listing8 from "../../Assets/listing8.png";
import listing9 from "../../Assets/listing9.png";
import listing10 from "../../Assets/listing10.png";
import listing11 from "../../Assets/listing11.png";
import listing12 from "../../Assets/listing12.png";
import listing13 from "../../Assets/listing13.png";
import listing14 from "../../Assets/listing14.png";
import listing15 from "../../Assets/listing15.png";
import listing16 from "../../Assets/listing16.png";
import listing17 from "../../Assets/listing17.png";
import listing18 from "../../Assets/listing18.png";
import listing19 from "../../Assets/listing19.png";
import listing20 from "../../Assets/listing20.png";
import listing21 from "../../Assets/listing21.png";
import listing22 from "../../Assets/listing22.png";
import listing23 from "../../Assets/listing23.png";
import listing24 from "../../Assets/listing24.png";
import { webSeriesJson } from "../../Utility/WebSeriesJson";

import Footer from "../Footer";
import Header from "../Header";

function WebseriesListingPage(props) {
  return (
    <React.Fragment>
      <Header />
      {/*<div className="listingPageHeader">
        <div style={{ display: "flex", alignItems: "center" }}>
          <h2 className="listingHeaderHeading">Lorem ipsum /</h2>
          <span className="listingHeaderSubHeading">Lorem ipsum</span>
        </div>
        <span className="listingHeaderContent">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
          rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
          ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
          sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
          dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
          et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
          takimata sanctus est Lorem ipsum dolor sit amet
        </span>
  </div>*/}
      <div className="listingList">
        {webSeriesJson &&
          webSeriesJson.map((val) => {
            return (
              <CardDivComp
                imgSrc={val.fld_image}
                divClass="newLaunchCard"
                hideSubContent={true}
                data={val.fld_plotsumprevseason}
                allData={val}
              />
            );
          })}
      </div>
      <Footer />
    </React.Fragment>
  );
}

export default WebseriesListingPage;
