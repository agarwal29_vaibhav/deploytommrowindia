import React from "react";
import CardDivCompMovie from "./Components/CardDivCompMovie";

import { MoviesJson } from "../../Utility/MoviesJson";
import Footer from "../Footer";
import Header from "../Header";

function ListingPage(props) {
  return (
    <React.Fragment>
      <Header />
      {/*<div className="listingPageHeader">
        <div style={{ display: "flex", alignItems: "center" }}>
          <h2 className="listingHeaderHeading">Lorem ipsum /</h2>
          <span className="listingHeaderSubHeading">Lorem ipsum</span>
        </div>
        <span className="listingHeaderContent">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
          rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
          ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
          sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
          dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
          et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
          takimata sanctus est Lorem ipsum dolor sit amet
        </span>
  </div>*/}
      <div className="listingList">
        {MoviesJson &&
          MoviesJson.map((val) => {
            return (
              <CardDivCompMovie
                imgSrc={val.fld_image}
                divClass="newLaunchCard"
                hideSubContent={true}
                data={val.fld_plot}
                allData={val}
              />
            );
          })}
      </div>
      <Footer />
    </React.Fragment>
  );
}

export default ListingPage;
