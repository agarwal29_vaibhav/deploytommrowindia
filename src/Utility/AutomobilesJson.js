export const AutomobilesJson = [
  {
    fld_name: "Strom R3",
    fld_tagline: "Perfect Electric Car for A Couple",
    fld_description:
      "When it comes to compact personal mobility Storm R3 is worth considering given its incredible range of 200km.\r\n\r\nWith a massive 400L combined space and measuring just 2.9m in length, the compact electric car is your perfect intra city commuting car.",
    fld_creative: "Yes",
    fld_generaldescription:
      "Do innovation, uniqueness, and sustainability excite you? Then Strom R3 is something which will bookmark your sight in the very first place. It's the first 3 wheeled car coming into the market with the range of 200 kms with just 3 hrs of charging.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Strom-R3 Fully Electric Air Conditioned\r\n2-Door 2-Seater with Large Sun-Roof\r\n\r\nLi-ion\r\n1,00,000 km or 3 Years Warranty (earliest)\r\n\r\nYes\r\n\r\n80 Kmph (Electronically Limited)\r\n\r\nTriple Touchscreen with Gesture and Voice Controls\r\n\r\nPremium Sound, Customizable Colors,\r\n3-year Free Maintenance\r\n\r\n200 Km\r\nEco/Normal/Sports\r\n\r\nRs. 4,50,000/-F1:G3",
    fld_keyfeatures:
      "Electric Car\r\nTouchscreen Interface\r\n2 door 2-Seater\r\nIOT Enabled\r\nRange 200 kms\r\nMileage Rs 0.40/Km",
    fld_fullspecifications:
      "Strom-R3 Fully Electric Air Conditioned\r\n2-Door 2-Seater with Large Sun-Roof\r\n\r\nLi-ion\r\n1,00,000 km or 3 Years Warranty (earliest)\r\n\r\nYes\r\n\r\n80 Kmph (Electronically Limited)\r\n\r\nTriple Touchscreen with Gesture and Voice Controls\r\n\r\nPremium Sound, Customizable Colors,\r\n3-year Free Maintenance\r\n\r\n200 Km\r\nEco/Normal/Sports\r\n\r\nRs. 4,50,000/-F1:G3",
    fld_variants:
      "Color: Blue, Red, Grey, white\r\nProduct offered in one variant",
    fld_specificitemsdesc: "Three wheeled Electric Vehicle",
    fld_expertreview:
      "https://www.youtube.com/watch?v=TWs98y_cI9c\r\n\r\nhttps://www.youtube.com/watch?v=VJLK_0trQRU\r\n\r\nhttps://www.youtube.com/watch?v=C_Xltj3v0C0",
    fld_gallery:
      "https://www.strommotors.com/#:~:text=FAQS-,GALLERY,-BROCHURE \r\n\r\n\r\nClick on Galllery for images provided by company itself ",
    fld_scheduledrelease: "Bookings Open",
    fld_blogs: "To be provided by Content Team",
    fld_faq:
      "What is Strom-R3?\r\nStrom-R3 is an urban mobility solution which is 100% electric and provides a safe and air-conditioned comfortable cabin for two. It is designed and built for every forward-thinking motorist, living in any modern city around the world.\r\n\r\nHow far can I go on a single charge?\r\nStrom-R3 comes in three fld_variants, with single charge range of 200 kms, 160 kms and 120 kms. Strom R3 has patent-pending proprietary AI-based range estimation algorithm gives you an accurate real-time max range, based on your driving pattern, traffic and road conditions. It puts to rest any range anxiety, if any.\r\n\r\nHow can you charge your Strom R3 car?\r\nStrom-R3 uses a Type-2 charge outlet which is compatible with most public chargers installed in India. It comes with an on-board high-speed charger which is also compatible with any wall outlet of 220V single phase supply. A single installation ofthe charging outlet at your residential parking spot is complimentary with every Strom R3 purchase. Subsequent installations will be easily available at a small extra fee.\r\n\r\nHow much time does it take to charge your Strom R3?\r\nStrom-R3 takes approximately 3-4 hours to charge using the on-board charger. Charge times may be affected by the ambient temperature and state of charge.\r\n\r\nWhat safety features does Strom-R3 offer?\r\nStrom-R3 uses a space frame design which is extremely rigid and lightweight. Its high-tensile steel is the same one used to make race car roll cages. Coupled with crash-worthy seats and a 3-point seat belt system, they make the platform extremely safe. In the company's internal crash tests, the platform has shown even better results than other cars, including SUVs in the market.\r\n\r\nHow and where do I go if my Strom-R3 needs servicing?\r\nIn the rare case of a component failure, we have a mobile service unit that can come to your location and replace the component with a service part. Strom-R3 is an electric vehicle which first and foremost means that there a lot less moving parts when compared to your internal conbustion engine powered cars. Hence, Strom-R3 requires minimal servicing and most of it can be done at your home by the company's team.\r\n\r\nHow can I pre-order or test drive the car?\r\nDue to the Covid-19 virus pandemic the company has had to delay their launch and pre-order schedules. The company will update these as soon as the situation normalizes. Please fill out the Contact Us forms at the website if you wish to get registered on their priority list. Pre-orders will be taken for the Mumbai Metropolitan Region. Do keep in mind the limited availability.\r\n\r\nHow do I get in touch with you?\r\nWrite to us at info@strommotors.com and they will get back to you ASAP.\r\n",
  },
  {
    fld_name: "Euler HiLoad",
    fld_tagline: "India’s 3- Wheeler Electric Hulk",
    fld_description:
      "\r\nPowered with six segment-first features this electric three-wheeler from Euler Motors can boast a load-carrying capacity of 688kg. \r\nThe Euler HiLoad EV is powered by a 12.4 kWh battery and has a certified range of 151 km on a single charge.",
    fld_creative: "Yes",
    fld_generaldescription:
      "Euler is focused on sustainable last mile transport through light electric commercial vehicles",
    fld_specificationname:
      "Big Load Capacity \r\nBig Battery\r\nRange \r\nFast Charge \r\nTorque",
    fld_specificationdescription:
      "688kg\r\n12.4kwh\r\n150kms\r\n50 km in 50 minutes\r\n88.55",
    fld_keyfeatures:
      "Projector headlamps \r\nDisc Brakes \r\n\r\nNo Toppling \r\nBattery Temperature \r\n\r\nOnboard Charger \r\nCharge on Wheels",
    fld_fullspecifications:
      'Big Load Capacity - 688kg\r\nBig Battery- 12.4kwh\r\nRange - 150kms \r\nFast Charge - 50 km in 50 minutes\r\nTorque - 88.55" "Projector headlamps \r\nDisc Brakes \r\nMore Tyre Width \r\n\r\nSafety\r\nNo Toppling \r\nBattery Temperature \r\nSafe Cabin \r\n\r\nCharging \r\nOnboard Charger \r\nExtensive Charging Network \r\nFast Charging \r\nCharge on Wheels',
    fld_variants:
      "Euler Hi Load 2200/Delivery Van \r\nEuler Hi Load 2200/Flat Deck Body \r\nEuler Hi Load 2200/High Deck Body \r\nEuler Hi Load 2200/Half Load Body",
    fld_specificitemsdesc: "Electric Vehicle for commercial uses",
    fld_expertreview:
      "https://www.youtube.com/watch?v=QxyelpHcoiA\r\n\r\nhttps://www.youtube.com/watch?v=kWciBkIYPnk\r\n\r\nhttps://www.youtube.com/watch?v=yt3tBr8LucQ",
    fld_gallery:
      "https://maryjane-assests.s3.ap-south-1.amazonaws.com/images/auto-hi-load.jpg",
    fld_scheduledrelease: 44562,
    fld_blogs: "To be provided by Content Team",
    fld_faq:
      "What is GVW of Euler Hi Load?\r\nGVW of a 3 Wheeler inclusive of kerb weight and payload of the vehicle. The GVW of Euler Hi Load is 1413 kg\r\n\r\nWhat is the engine capacity of Euler Hi Load?\r\nThe engine capacity of the 3 Wheeler is the maximum power & maximum torque. The maximum power of Hi Load is 14 hp , maximum torque is 88.55 Nm.\r\n\r\nWhat is the Wheelbase of Euler Hi Load?\r\nThe wheelbase of Euler Hi Load is 2200 mm\r\n\r\nWhat is the body & chassis configuration of Euler Hi Load?\r\nEuler Hi Load is available in Box body option. The cabin type of Hi Load is a Day Cabin & chassis type is a Chassis with Cabin .",
  },
  {
    fld_name: "Okinawa I Praise +",
    fld_tagline: "Will This E-scooter Garner Praise in Indian Market ?",
    fld_description:
      "The perfect combo of artistic design with practical features makes it one of the most demanding e-scooter in India.\r\nRiding this e-scooter is a joy as unlike other scooters, it can make your hair stand with its blazing fast top speed of 75kmph.\r\nInvest in this e-scooter and break free from the clutches of expensive fuel.",
    fld_creative: "Yes",
    fld_generaldescription:
      "Okinawa has launched it's E scooter Praise+ in Indian market to target Indian electric two wheeler segment. It's elegant, stylish and pocket friendly as well.",
    fld_specificationname:
      "Design - Aerodynamic Design\r\nRange - 139km\r\nElectric battery - Li-ion\r\nCharging time - 2-3 hrs\r\nSpeed - NA\r\nPrice - Rs. 76,848\r\nMaximum Load - 150kg\r\nMotor power - 1000 watt\r\nBrakes - Disc\r\nWarranty - 3 yrs",
    fld_specificationdescription:
      "Design - Stylish\r\nRange - 139km\r\nElectric battery - Li-ion\r\nCharging time - 2-3 hrs\r\nSpeed - NA\r\nPrice - Rs. 76,848\r\nMaximum Load - 150kg\r\nMotor power - 1000 watt\r\nBrakes - Disc\r\nWarranty - 3 yrs",
    fld_keyfeatures:
      "Service Due Indicator\r\nLED Tail Lights\r\nSpeedometer Digital\r\nOdometer Digital\r\nTripmeter Digital\r\n\r\nMobile Application \r\nGeofencing \r\nDigitalPass Switch\r\nAnti Theft Alarm\r\nNavigation\r\nEBS\r\nImmobilization\r\nTrips \r\nSecure Parking\r\nTracking & Monitoring\r\nSpeed Alerts \r\nDriver Score\r\nICAT/ARAI Approved Display",
    fld_fullspecifications:
      "Mobile Charging USB Port \r\nStylish Tail lamp \r\nAerodynamic design \r\nFront LED Headlamp \r\nLarge foot space with body coloured foot mat",
    fld_variants:
      "Glossy red Black \r\nGlossy golden black \r\nGlossy Silver Black",
    fld_specificitemsdesc:
      "Length/Width/Height 1970/ 745/1165 mm\r\nGeo-Fencing\r\nAllows the user to set a virtual perimeter, ranging from 50 m to 10 km, and to receive an alert anytime the vehicle moves beyond this space. It ensures vehicle safety from unauthorized movement.\r\n\r\nVirtual Speed Limit\r\nThe ‘speed limit’ feature in i-Praise helps users to set speed thresholds for the vehicles. This especially helps parents to immediately get an alert message on the app to ensure they do not over-speed.\r\n\r\nCurfew Hours\r\nFor those working late night hours or are sure to be occupied during a long meeting, the e-scooter’s curfew feature helps to set ‘no ride hours’ which will continue for the defined period till the user sets it off.\r\n\r\n",
    fld_expertreview:
      "https://www.youtube.com/watch?v=zxrvYqDS5LY\r\nhttps://www.youtube.com/watch?v=wNSPplULKWo\r\nhttps://www.youtube.com/watch?v=8PXSe7hShpA\r\nhttps://www.youtube.com/watch?v=OEgC0qi3rvI\r\nhttps://www.autocarindia.com/bike-reviews/2019-okinawa-praisepro-review-test-ride-414408\r\nhttps://www.bikedekho.com/okinawa/praisepro",
    fld_gallery: "Bookings Open",
    fld_scheduledrelease:
      "Q: What is the on-road price of Okinawa i-Praise in 2021?\r\nA: The 2021 on-road price of Okinawa i-Praise in Delhi is Rs. 1,10,059. This on-road price includes the ex-showroom price, RTO and insurance charges.\r\n\r\nQ: Which is better Okinawa i-Praise or Okinawa Praise?\r\nA: Okinawa i-Praise is priced at Rs. 1,07,233, has a top speed of 58 Kmph, range of 139 Km and takes 4-5 Hrs hours to charge, whereas, the price of Okinawa Praise is Rs. 80,571 with a top speed of 58 Kmph, giving a range of 88 Km and a charging time of 2-3 Hrs. \r\n\r\nQ: What are the colour options of Okinawa i-Praise?\r\nA: Okinawa i-Praise is available in 3 colours which are Glossy Red Black, Glossy Silver Black and Glossy Golden Black. You can check all the colour images of Okinawa i-Praise.\r\n\r\nQ: What are the key specifications of Okinawa i-Praise?\r\nA: Okinawa i-Praise is an electric bike that comes with a 1000 W motor which gives a range of 139 Km, and a top speed that goes uptil 58 Kmph. It has a charging time of 4-5 Hrs. You can check the fld_fullspecifications and features.",
  },
  {
    fld_name: "Mini Cooper SE",
    fld_tagline: "Mini’s Legend Gets Electric Avatar",
    fld_description:
      "The legendary Mini Copper gets an electric avatar that has updated fresh technological and stylish features without guzzling tons of fuel. \r\nThe car is loaded with a 32.6kWh battery pack that can make the car sprint from 0-100kmph in 7.3 seconds and up to an astonishing 150kmph. \r\n",
    fld_creative: "Yes",
    fld_generaldescription:
      "Mini India teased the upcoming all-electric Cooper SE in the country ahead of its launch that is expected to take place in the coming weeks. The company also began accepting bookings for its first electric model in India inNovember for an amount of Rs 1 lakh.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Mini Cooper SE\r\n\r\nLi-ion\r\n8 years/100,000 miles warranty\r\n\r\nYes \r\n\r\n150kmph\r\n\r\nRailing and roof rack systems, holders for tablets or hangers and universal hooks for bags.\r\n\r\n233km per charge\r\n\r\nSport, Mid, Green, and Green+\r\n\r\nRs.50.00 Lakh*",
    fld_keyfeatures:
      "Multifunctional instrument Display\r\nNew Front bumper design \r\nExclusive Side Scuttle Design \r\nNew Rear Bumper Design \r\n",
    fld_fullspecifications:
      "Fuel Type Electric\r\nMax Power (bhp@rpm) 181.03bhp\r\nSeating Capacity 5 \r\nTransmission Type Automatic\r\nBody Type Hatchback\r\n",
    fld_variants:
      "3 Door Hatch Cooper SE\r\n3 door Hatch\r\nConvertible \r\nCountryman \r\nJohn Cooper Works \r\n\r\nColors\r\nMoonwalk Grey\r\nWhite Silver \r\nBritish Racing Green \r\nMidnight Black ",
    fld_specificitemsdesc:
      "211 Litres - Space is of the essence. If you drop the 60/40 split rear seats you get more room with up to 731 litres of cargo space – letting you take almost everything with you.\r\n\r\n",
    fld_expertreview:
      "https://www.youtube.com/watch?v=A1CUa0FwtGc\r\nhttps://www.youtube.com/watch?v=uFTjo3dq_oc\r\nhttps://www.youtube.com/watch?v=6BsHJV4_1OM",
    fld_gallery: 44593,
    fld_scheduledrelease:
      "What is the expected price of Mini Cooper SE?\r\nThe price of Mini Cooper SE is expected to be Rs. 50.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Mini Cooper SE?\r\nThe estimated launch date of Mini Cooper SE is Dec 14, 2021.\r\n\r\nWhat are the expected key specifications of Mini Cooper SE?\r\nMini Cooper SE will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Mini Cooper SE?\r\nMini Cooper SE will be competed with Superb, Q2, Carnival, 5 Series, S5 Sportback.",
  },
  {
    fld_name: "Mahindra eKuv100",
    fld_tagline: "Is This Mahindra’s “Think Different” Moment?",
    fld_description:
      "Get Ready for this homegrown electric powerhouse that will transform how you use your car for your daily commute. Paired with a powerful 15.9Kwh that churns out 54.4 bhp of power and a fast charger that can charge 80% in less than an hour, this electric car is the top contender for the most popular e-cars in India.",
    fld_creative: "Yes",
    fld_generaldescription:
      "With the Trust of Mahindra and Made in India, Mahindra Ekuv100 is something which will satisfy you from every aspects of a car. And within this price range it's a good pick for everyone.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Mahindra eKuv 100\r\n\r\nlithium-ion\r\n\r\nYes\r\n\r\n85km/hr\r\n\r\nN/A\r\n\r\n147km\r\n\r\nSport, Eco\r\n\r\nRs.8.25 Lakh*",
    fld_keyfeatures:
      "Mahindra eKUV100 is an Electric suv Car. The electric motor generates a power of 54PS and a torque of 120Nm. Mahindra eKUV100 is available in Automatic transmission only. \r\n",
    fld_fullspecifications:
      "Fuel Type - Electric \r\nPower - 54PS\r\nBody Type - Full SUV\r\nSeats - 5",
    fld_variants: "Color - White",
    fld_specificitemsdesc: "NA",
    fld_expertreview: "4.25 - Cardekho",
    fld_gallery: 44621,
    fld_scheduledrelease: "Latest Update \r\n",
    fld_blogs:
      "What is the expected price of Mahindra eKUV100?\r\nThe price of Mahindra eKUV100 is expected to be Rs. 8.25 Lakh*.\r\n\r\nWhat is the estimated launch date of Mahindra eKUV100?\r\nThe estimated launch date of Mahindra eKUV100 is Mar 01, 2022.\r\n\r\nWhat are the expected key specifications of Mahindra eKUV100?\r\nMahindra eKUV100 will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Mahindra eKUV100?\r\nMahindra eKUV100 will be competed with Santro, GO Plus, Grand i10 Nios, i20 N Line, Kicks.",
  },
  {
    fld_name: "Tata Tiago EV",
    fld_tagline: "Tata is Betting Big on This EV!",
    fld_description:
      "Showcased at the 2018 Auto Expo event, this EV car is expected to be seen soon on the Indian roads.\r\nThis 5 seater hatchback will get a fast-charging capability and a driving range of around 130km.\r\nThis EV car has a top speed of 100kmph and gets features like airbags, ABS with EBD, Vehicle Stability Control, and a top speed of 100kmph.",
    fld_creative: "Yes",
    fld_generaldescription:
      "Gorgeous looking beautiful car with the trust of TATA. A feature loaded car along with sustainable ride and handling dynamics make the Tiago a good EV hatchback.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Tata Tiago EV\r\n\r\n30kW electric motor and a 216Ah battery pack\r\n\r\nYes \r\n\r\n1000 kmph in \r\n\r\nDigital \r\n\r\nYes\r\n\r\n130km\r\n\r\nAutomatic\r\n\r\nRs. 6 Lakh",
    fld_keyfeatures:
      "Safe and Reliable \r\nWaterproof IP67 rated battery pack and motor \r\n8 year battery and motor warranty \r\nDriven and Proven for over 35 mn kms\r\n\r\nAdvanced technology \r\nLiquid cooled Li-ion Battery \r\n60 minutes fast charging time\r\n30+ Coonected car features \r\n\r\nUnmatched Performance\r\nInstant Torque \r\n0-60 km/h under 6 Seconds \r\nPermanent Magnet Synchronus Motor ` ",
    fld_fullspecifications:
      "Engine and Transmission\r\n\r\nTransmission Type - Automatic \r\n\r\nFuel and Performance\r\n\r\nFuel Type- Electric \r\nEmission Norm - BS VI\r\n\r\nCharging \r\n\r\nFast Charging - NA\r\n\r\nDimensions and Capacity\r\nSeating Capacity-5 \r\nNo of Doors - 5",
    fld_variants: "Colour - White",
    fld_specificitemsdesc: "Powered by Ziptron",
    fld_expertreview:
      "https://www.youtube.com/watch?v=HPzDFzy-Mu8\r\nhttps://www.youtube.com/watch?v=KtLzdO1vUQg",
    fld_gallery: 44531,
    fld_scheduledrelease: "NA",
    fld_blogs:
      "What is the expected price of Tata Tiago EV?\r\nThe price of Tata Tiago EV is expected to be Rs. 6.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Tata Tiago EV?\r\nThe estimated launch date of Tata Tiago EV is Mar 01, 2022.\r\n\r\nWhat are the expected key specifications of Tata Tiago EV?\r\nTata Tiago EV will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Tata Tiago EV?\r\nTata Tiago EV will be competed with Tiago, GO, Wagon R, Amaze, Thar.\r\n\r\nDoes the Tata Tiago EV have a sunroof?\r\nTata Tiago EV does not have a sunroof.",
  },
  {
    fld_name: "Tesla Model 3",
    fld_tagline: "Blend of Marvelous Engineering and Futuristic Design",
    fld_description:
      "Tesla cars don't need an introduction but the features do as they are not only advanced but some are Tesla exclusive only.\r\nWith never before heard features like phone key unlock, custom driving profile, glas roof and famed autopilot mode Tesla model 3 is driving the world crazy.\r\nWith an insane driving range of 500+km, Tesla 3 is the king of upcoming electric cars.",
    fld_creative: "Yes",
    fld_generaldescription:
      "The Model 3 is a desirable and relatively affordable all-electric car that has helped the Tesla brand push EVs into the mainstream.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Tesla Model 3\r\n\r\nLithum- ion\r\n\r\nYes\r\n\r\n261kmph\r\n\r\nA massive 15-inch touchscreen infotainment system\r\n\r\nGlobal Upgrades Available \r\n\r\n498 km\r\n\r\nAutomatic, Autonomous Driving \r\n\r\nRs. 60 Lakh (expected)",
    fld_keyfeatures:
      "Bluetooth Keyless Entry- With this feature, in the model 3 you can unlock your car using your android/ios devices. \r\n\r\nCustom Driver Profile \r\nHidden Air Vents \r\nGlass Roof \r\nHiddne Games, Easter Eggs \r\nFront Trunk \r\n50:50 Split Seats \r\nSentry Mode",
    fld_fullspecifications:
      "Motor Type - permanent-magnet synchronous AC\r\nTRANSMISSION: 1-speed direct drive\r\nDIMENSIONS: \r\nWheelbase: 113.2 in \r\nLength: 184.8 in \r\nWidth: 72.8 in Height: 56.8 in \r\nPassenger volume: 97 cu ft \r\nTrunk volume: 15 cu ft \r\nCurb weight: 3897 lb\r\nC/D FUEL ECONOMY: \r\n75-mph highway driving: 84 MPGe \r\nHighway range: 200 miles \r\n\r\nEPA FUEL ECONOMY: \r\nCombined/city/highway: 130/136/123 MPGe \r\nRange: 310 miles",
    fld_variants:
      "Tesla Model 3, the electric car is offered in three fld_variants - Performance, Long Range AWD & Standard Plus. The performance variant is capable of hitting 0 to 60 mph in just 3.6 seconds whereas the standard & long-range fld_variants take 5.3 seconds and 4.4 seconds, respectively.",
    fld_specificitemsdesc:
      "Infotainment and Connectivity\r\nNavigation, Bluetooth, and USB connectivity are standard, but Model 3 is not available with AM radio or SiriusXM satellite radio. Several internet-streaming radio options are standard. It also can't be equipped with Android Auto or Apple CarPlay.\r\n\r\nSafety and Driver-Assistance Features\r\nThe Model 3 has been crash-tested by the National Highway Traffic Safety Administration (NHTSA) and received a perfect five-star score; the Insurance Institute for Highway Safety (IIHS) has named it a Top Safety Pick+.\r\n\r\nWarranty and Maintenance Coverage\r\nTesla's overall warranty coverage is among the longest in the EV segment, but the coverage doesn't include corrosion protection. The Long Range model includes an additional 20,000 miles on the coverage period of its battery and drive components.",
    fld_expertreview:
      "https://www.youtube.com/watch?v=kbulCM90w8w\r\nhttps://www.youtube.com/watch?v=PtxMYGxQ9Zc\r\nhttps://www.youtube.com/watch?v=3YI7Lxop0n8\r\nhttps://www.youtube.com/watch?v=kHw3bfydrLM",
    fld_gallery: 44531,
    fld_scheduledrelease:
      "What is the Expected Price of Tesla Model 3 ?\r\nThe Price of Tesla Model 3 is expected to be Rs.75 Lakh.\r\n\r\nWhat is the Estimated Launch date of Tesla Model 3 ?\r\nThe Estimated Launch date of Tesla Model 3 is Dec 24, 2021.\r\n\r\nWhat are the Expected Key Specifications of Tesla Model 3 ?\r\nTesla Model 3 will be a sedan available in Automatic transmission and Electric Car options.",
  },
  {
    fld_name: "Tesla Model S",
    fld_tagline: "Why Tesla Model S Will Raise The EV Bar?",
    fld_description:
      "The Tesla Model S is built technologically superior and to satisfy individuals who don’t want to shell tons of money for pure performance.\r\nFamed for its massive infotainment display, an airplane-like cockpit with autopilot driving assistance, the Model S is back with improved performance and design.\r\n",
    fld_creative: "Yes",
    fld_generaldescription:
      "Once the jewel of the Tesla lineup, the Model S is finally being challenged by more luxurious rivals, but it's still a formidable luxe-class EV.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Tesla Model S\r\n\r\nLithium Ion Traction Battery w/11.5 kW Onboard Charger, 12 Hrs Charge Time @ 220/240V and 100 kWh Capacity\r\n\r\n321.9 kmph\r\n\r\nDigital and Touch \r\n\r\nGlobally Available \r\n\r\n524 km\r\n\r\nautomatic, four wheel drive \r\n\r\nRs. 1.50cr",
    fld_keyfeatures:
      "Autopilot \r\nLudicrous Plus Mode\r\nSuperchargers \r\nOver the air Updates \r\nStreaming Services \r\nWeb Browser \r\nAuto Raising Suspension \r\nAdvanced Parking Censors \r\nThe Key \r\nThe App \r\nEaster Eggs",
    fld_fullspecifications:
      "VEHICLE TYPE\r\nfront- and mid-motor, all-wheel-drive, 5-passenger, 4-door hatchback\r\n\r\n\r\nMOTORS\r\nfront: permanent-magnet synchronous AC, 275 hp, 310 lb-ft; rear: induction AC, 503 hp, 531 lb-ft; 98.0-kWh lithium-ion battery pack\r\n\r\nTRANSMISSION\r\nsingle-speed direct drive\r\n\r\nCHASSIS\r\nSuspension (F/R): multilink/multilink\r\nBrakes (F/R): 14.0-in vented disc/14.4-in vented disc\r\nTires: Michelin Pilot Sport 4S, F: 245/35R-21 96Y TO R: 265/35R-21 101Y TO\r\n\r\nDIMENSIONS\r\nWheelbase: 116.5 in\r\nLength: 195.7 in\r\nWidth: 77.3 in\r\nHeight: 56.9 in\r\nPassenger volume: 95 ft3\r\nCargo volume: 26 ft3\r\nCurb weight: 4998 lb",
    fld_variants:
      'Tesla has sold the following versions of the Model S: 60, 60D, 70, 70D, 85, 85D, P85, P85D, 90D, P90D, 100D, and P100D. The "D" in the name refers to all-wheel-drive models, and the "P" refers to high-performance versions.',
    fld_specificitemsdesc:
      "Range, Charging, and Battery Life\r\nUnder the Tesla's floor lies a battery pack that yields a low center of gravity and evenly distributes weight from front to rear. Driving range and acceleration performance varies from model to model, with the Long Range version's battery providing up to a 412-mile range while the Plaid model offers up to 390.\r\n\r\nFuel Economy and Real-World MPG\r\nWhile EVs such as the Chevy Bolt and Polestar 2 deliver serviceable driving range the Model S remains an impressive alternative to gasoline-powered vehicles when it comes to long-distance usability. The base Long Range model receives the highest MPGe ratings at 121 city and 112 highway. In our real-world, 75-mph highway fuel economy test, a 2020 Model S posted a 222-mile highway range number against its 326-mile EPA estimated driving range.",
    fld_expertreview:
      "https://www.youtube.com/watch?v=A5PIgmXGIdI\r\nhttps://www.youtube.com/watch?v=XKzO9HQgRyE\r\nhttps://www.youtube.com/watch?v=WInTHMAj2ZU\r\nhttps://www.youtube.com/watch?v=qen0ZlZM0ZA",
    fld_gallery: 44531,
    fld_scheduledrelease:
      "What is the expected price of Tesla Model S?\r\nThe price of Tesla Model S is expected to be Rs. 1.50 Cr*.\r\n\r\nWhat is the estimated launch date of Tesla Model S?\r\nThe estimated launch date of Tesla Model S is Mar 01, 2023.\r\n\r\nWhat are the expected key specifications of Tesla Model S?\r\nTesla Model S will be a sedan available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Tesla Model S?\r\nTesla Model S will be competed with Phantom, Ghost, Rolls Royce Dawn, SF90 Stradale, Rolls Royce Wraith.",
  },
  {
    fld_name: "Ora iQ",
    fld_tagline: "Will This EV from China Mesmerize Indian Hearts?",
    fld_description:
      "The Chinese car engineering is growing by leaps and bounds when it comes to electric battery technology.\r\nA Chinese crossover electric car manufactured by the Great Wall Motors that houses battery capacity of 46.57 kWh gives the car a driving range of 350km.\r\nThis car takes all its juice to reach upto a max speed of 150kmhr.",
    fld_creative: "Yes",
    fld_generaldescription:
      "It is expected be the most beautiful and amazing designed car. An advantage of the car is automatic gear box, windows on the roof, 8 LED bulb and also have alloy wheel. The car has electric control wind mirror.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Ora IQ\r\n\r\n46.57 Kwh\r\n\r\nNo \r\n\r\n150 Kmh\r\n\r\nDigital \r\n\r\nAvailable \r\n\r\n401 Km\r\n\r\nAutomatic \r\n\r\nRs. 20 Lakh",
    fld_keyfeatures:
      "Large Space - Great wall ORA ME exclusive electric platform adopts the design concept of short body, long wheelbase, four wheels and four corners, which can bring larger interior space. \r\n\r\nHigh Safety Standards - ME platforms integrates the battery with the chasis, which not only makes the overall centre of the gravity of the vehicle 15% lower than that of the fuel vehicle, but also ensures the integrity of the battery pack during collison and improves the safety of vehicle\r\n\r\nFull Intelligence - The electrical architecture is divided into four domains of autopilot, audio and video, car body and power for management, with functions including mobile phone key, smart ID and speech recognition. \r\n\r\nLong Mileage - ORA built on ME platform has effectively improved the vehicle's maneuverability, runs more stable, consumes less power and has longer mileage.",
    fld_fullspecifications:
      "Brand ORA\r\nModel iQ\r\nGeneration iQ\r\nModification (Engine) 46.57 kWh (163 Hp)\r\nStart of production 2018 year\r\nPowertrain Architecture Electric Vehicle\r\nBody type SUV\r\nSeats 5\r\nDoors 5\r\nPerformance specs\r\nFuel Type Electricity\r\nAcceleration 0 - 100 km/h 7.7 sec\r\nAcceleration 0 - 62 mph 7.7 sec\r\nAcceleration 0 - 60 mph (Calculated by Auto-Data.net) 7.3 sec\r\nMaximum speed 150 km/h\r\n93.21 mph\r\nElectric cars and hybrids specs\r\nBattery capacity 46.57 kWh\r\nAll-electric range 401 km\r\n249.17 mi\r\nAverage Energy consumption 13.1 kWh/100 km\r\n21.08 kWh/100 mi\r\n160 MPGe\r\n7.6 km/kWh\r\n4.7 mi/kWh\r\nElectric motor 1\r\nElectric motor power 163 Hp\r\nElectric motor Torque 280 Nm\r\n206.52 lb.-ft.\r\nSystem power 163 Hp\r\n\r\n2018 ORA iQ 46.57 kWh (163 Hp) | Technical specs, data, fuel consumption, Dimensions: https://www.auto-data.net/en/ora-iq-46.57-kwh-163hp-37969",
    fld_variants: "NA",
    fld_specificitemsdesc: "NA",
    fld_expertreview:
      "https://www.youtube.com/watch?v=ik98jTpKIO8\r\nhttps://www.youtube.com/watch?v=ik98jTpKIO8",
    fld_gallery: 44531,
    fld_scheduledrelease:
      "What is the expected price of ORA iQ?\r\nThe price of ORA iQ is expected to be Rs. 20.00 Lakh*.\r\n\r\nWhat is the estimated launch date of ORA iQ?\r\nThe estimated launch date of ORA iQ is Apr 01, 2022.\r\n\r\nWhat are the expected key specifications of ORA iQ?\r\nORA iQ will be a sedan available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of ORA iQ?\r\nORA iQ will be competed with Hector, Safari, XUV700, Alcazar, Superb.",
  },
  {
    fld_name: "Tata Sierra",
    fld_tagline: "Will Tata Sierra Achieve its Past Glory?",
    fld_description:
      "Unveiled at the 2020 Auto Expo, this electric avatar of Tata Sierra has some unique features like panoramic view with an additional sliding door for access to rear lounge like seating arrangement. The car is said to be based on flexible ALFA platform that might be reality in 2022-2023.",
    fld_creative:
      "The car is so beautifully designed by Tata Motors. The features that come in this segment are awesome and the side view mirror design is very nice along with a beautiful interior.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Sierra\r\n\r\nLi-Ion\r\n\r\nYes \r\n\r\nNA\r\n\r\nTouch\r\n\r\nAvailable \r\n\r\n500km\r\n\r\nManual Transmission \r\n\r\nRs. 14 Lakh",
    fld_specificationdescription: "Electric Car\r\n5 Seater SUV\r\n",
    fld_keyfeatures: "NA",
    fld_fullspecifications: "NA",
    fld_variants:
      "Developed in United Kingdom The new Tata Sierra EV concept might draw a lot of inspiration from the original Sierra SUV, but it has been developed by a Tata Motors European Technical Centre (TMETC) design team based in Coventry, United Kingdom.\r\n\r\nThe original Sierra had large glass panels on the side, and the Sierra EV concept has taken that trend a step further. The concept car is equipped with an all-glass rear compartment, which would make being seated in the backseat of the car a serene experience, especially when it’s raining.\r\n\r\nA beacon light has been placed just behind the B-pillar, and it further enhances the lounge-like experience of the rear compartment.",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=oFYMyyHXbyc\r\nhttps://www.youtube.com/watch?v=FcTDNDzoRzM\r\nhttps://www.youtube.com/watch?v=46mCIeNKJKE",
    fld_expertreview: 44562,
    fld_gallery:
      "What is the expected price of Tata Sierra?\r\nThe price of Tata Sierra is expected to be Rs. 14.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Tata Sierra?\r\nThe estimated launch date of Tata Sierra is Mar 01, 2023.\r\n\r\nWhat are the expected key specifications of Tata Sierra?\r\nTata Sierra will be a suv available in Manual transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Tata Sierra?\r\nTata Sierra will be competed with Venue, XUV300, Duster, Elantra, Astor.",
  },
  {
    fld_name: "Maruti WagonR Electric",
    fld_tagline: "Tall Boy In Electric Avatar!",
    fld_description:
      "Maruti-Suzuki's first electric car is revealed! The tall boy or commonly known as Maruti Wagon R spotted on roads during it's developement mule. \r\nThis EV car is speculated to come with subtle variations to its exterior styling including new bumpers. The prototype receives all-LED lighting with a sharp-looking split headlamp design at the front.",
    fld_creative: "Yes",
    fld_generaldescription:
      "The new Wagon R EV looks very different from the existing car. It comes with a new front end design and a two-tier light console. While the indicators are positioned on top, the headlamp console and the fog lights sit on the front bumper.",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Wagon R Electric \r\n\r\nElectric \r\n\r\nNA\r\n\r\nNA\r\n\r\nNA\r\n\r\nNA\r\n\r\n90 Kms\r\n\r\nautomatic \r\n\r\nRs. 9 Lakh",
    fld_keyfeatures:
      "Steering column \r\nFog lamps \r\nChild safety lock \r\nAnti-theft system \r\nVentilated disc brakes \r\nFront and drum brakes at the rear \r\nDual airbags \r\nABS with EBD",
    fld_fullspecifications: "Not Known",
    fld_variants: "NA",
    fld_specificitemsdesc:
      "Performance & Handling\r\nMaruti has already employed a number of Wagon R electric vehicles for testing all over the country. The performance and handling of the vehicle can only be speculated for now.\r\n\r\nMileage\r\nThe Maruti WagonR Electric’s mileage will stand at 90 kilometres per full charge. The overall mileage might narrow down to the maintenance of battery and other factors as well. In general, a mileage of 60 to 80 km per full charge can be expected.\r\n\r\nBraking & Safety\r\nThe braking system of the EV is quite sufficient. The body structure of the car is energy-absorbing, which might reduce the collision impacts.\r\n\r\nApart from that ABS and EBD is standard. The adjustable steering column, alarm system and the computerized anti-theft system are also expected to be standard.\r\n\r\nPrice\r\nComing to the main point of concern, the price. Maruti WagonR Electric is expected to sit at the 7 lakh mark. This is only if the state and central government subsidies apply. Without those, the car might go near the mark of 10 lakhs.",
    fld_expertreview:
      "https://www.cardekho.com/maruti/wagonr-electric/user-reviews",
    fld_gallery: 44562,
    fld_scheduledrelease:
      "What is the expected price of Maruti WagonR Electric?\r\nThe price of Maruti WagonR Electric is expected to be Rs. 10.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Maruti WagonR Electric?\r\nThe estimated launch date of Maruti WagonR Electric is Not Yet Announced.\r\n\r\nWhat are the expected key specifications of Maruti WagonR Electric?\r\nMaruti WagonR Electric will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Maruti WagonR Electric?\r\nMaruti WagonR Electric will be competed with GO Plus, Figo, Grand i10 Nios, i20 N Line, Kicks.",
  },
  {
    fld_name: "Tesla Model X",
    fld_tagline: "Will Tesla Model X Disrupt EV industry? Let's check out!",
    fld_description:
      "Tesla Model X is credited as one of the first electric luxury SUVs ever made in the world, and is all set to change the game yet again. \r\nThe car gets refreshed exterior and interior, and a three-motor plaid variant. With over 1,000 horsepower, Tesla Model X is well -primed to take on electric SUV newcomers including the Rivian R1S and Lucid Gravity.\r\n",
    fld_creative: "Yes",
    fld_generaldescription:
      "Among electric SUVs, the Model X reigns supreme in driving range, ",
    fld_specificationname:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationdescription:
      "Model X\r\n\r\nLi-ion 75 kWh\r\n\r\nYes\r\n\r\n155mph\r\n\r\nTouch \r\n\r\nYes \r\n\r\n579km\r\n\r\nAutomatic, Autopilot, Four wheel drive\r\n\r\nRs. 2cr",
    fld_keyfeatures:
      "Falcon wing rear doors\r\nAutomatic Closing front doors \r\nAuto Summon \r\nAutopilot \r\nRange and Pricing \r\nConvenience\r\nBrake Hold\r\nCenter Stack Controls",
    fld_fullspecifications:
      "EPA Classification-Standard SUV 4WD\r\nDrivetrain-All Wheel Drive\r\nEngine Type and Required Fuel-Electric\r\nTransmission fld_description- Automatic\r\nMOTOR TYPE: 2 asynchronous AC\r\n\r\nPOWER:\r\nF: 259 hp R: 503 hp\r\nCombined: 532 hp\r\nTorque: F: 244 lb-ft R: 469 lb-ft\r\nCombined: 713 lb-ft\r\n\r\nTRANSMISSION: 1-speed direct-drive\r\n\r\nDIMENSIONS:\r\nWheelbase: 116.7 in\r\nLength: 198.3 in\r\nWidth: 81.5 in Height: 66.3 in\r\nSAE volume: F: 60 cu ft M: 52 cu ft R: 28 cu ft\r\nCargo volume (third row up/folded): 13/68 cu ft\r\nFront trunk: 7 cu ft\r\nCurb weight: 5594 lb",
    fld_variants:
      "The Tesla Model X is currently available in two versions: a dual motor, all-wheel drive (AWD) Long Range trim, and a new tri motor plaid powertrain. Both trims offer Tesla's best 91 cubic feet of cargo space.",
    fld_specificitemsdesc:
      "Engine, Transmission, and Performance\r\nLong Range Model Xs come standard with two electric motors—one at the front axle and one at the rear—which enables all-wheel-drive capability. This setup is plenty swift and Tesla claims a zero-to-60-mph time of 3.8 seconds.\r\n\r\nRange, Charging, and Battery Life\r\nThe Model X comes standard with a battery large enough to cover a claimed 360-mile driving range; upgrading to the faster Plaid model drops the estimated driving range to 340 miles. \r\n\r\nFuel Economy and Real-World MPG\r\nAmong other similarly-sized all-electric SUVs, the Model X boasts the highest MPGe ratings from the EPA. Long Range models earned ratings of 109 city, 101 highway, and 105 MPGe combined. \r\n\r\nInterior, Comfort, and Cargo\r\nA stark and nearly buttonless interior design is classic Tesla. The Model X's unique windshield/glass roof runs seamlessly from the base of the hood up over the front-seat passengers for a nearly uninterrupted view of what's ahead and what's above.",
    fld_expertreview:
      "https://www.youtube.com/watch?v=tOczmJSLvug\r\nhttps://www.youtube.com/watch?v=QwSXuAMcRT8\r\nhttps://www.youtube.com/watch?v=aKra-KgUJIU",
    fld_gallery: 44562,
    fld_scheduledrelease:
      "What is the expected price of Tesla Model X?\r\nThe price of Tesla Model X is expected to be Rs. 2.00 Cr*.\r\n\r\nWhat is the estimated launch date of Tesla Model X?\r\nThe estimated launch date of Tesla Model X is Mar 01, 2023.\r\n\r\nWhat are the expected key specifications of Tesla Model X?\r\nTesla Model X will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Tesla Model X?\r\nTesla Model X will be competed with Phantom, Ghost, Rolls Royce Dawn, SF90 Stradale, Rolls Royce Wraith.",
  },
  {
    fld_name: "Volvo XC40 Recharge",
    fld_tagline: "Volvo Blasts into EV World With 400KM Driving Range",
    fld_description:
      "Volvo’s first pure electric compact SUV is and engineered for urban driving that gets you 408 bhp of power and 400+km of driving range. Volvo extreme engineering makes it possible to charge 80% of battery in mere 40 mins.",
    fld_creative:
      "The XC40 Recharge has a 78kWh battery pack that provides a WLTP certified range of 418km on a full charge. All the power is sent to all four wheels via two electric motors.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Volvo XC40 Recharge\r\n\r\n78kwh\r\n\r\nNA\r\n\r\n180kmph\r\n\r\nNine inch Android Based Touch Screen \r\n\r\nNA\r\n\r\n418\r\n\r\nAutomatic \r\n\r\nRs. 50-55 Lakh (expected)\r\n",
    fld_specificationdescription:
      "Both responsive and responsible driving – the vital combination provided by Volvo becomes the company's first pure electric luxury SUV.\r\n\r\nPure power\r\nDriving is smooth and intuitive. Just take your seat, select gear and go – there’s no start button. Then enjoy the refined power of twin electric motors and all-wheel drive, with zero tailpipe emissions.\r\n\r\nTake charge\r\nEnjoy the freedom of electric driving with a projected range of up to 418km/208 miles*. When needed, you easily top up at home or at work – or fast-charge from zero to 80% in around 40 minutes**",
    fld_keyfeatures:
      "Engine\r\nType Electric motor front / rear\r\nElectrical motor max output, kW (kW/engine) (hp)/rpm (metric) 300 (150x2)(408)/ 4350-13900\r\nElectrical motor max output, kW (kW/engine) (hp)/rpm (mechanical) 300 (150x2)(402)/ 4350-13900\r\nMax torque, Nm (Nm/engine)/rpm) 660 (330x2)/up to 4350\r\nPerformance\r\nAcceleration, 0-100 km/h (sec) 4.9\r\nAcceleration, 0-60 mph (sec) 4.7\r\nTop speed, km/h 180\r\nTop speed, mph 112\r\nBattery\r\nBattery content, total amount of energy, (kWh) (Nominal) 78\r\nBattery content, total amount of energy, (kWh) (Usable) 75\r\nCharging\r\nAC, h (11 kW) (h) 8\r\nDC, min (150 KW) ** (min) 1 40\r\n1 **(0-80% loading rate, battery temp 20-35 deg C)\r\nRange\r\nElectric Range, (WLTP) (Km) (EU) 400-418\r\nElectric Range, (EPA) (Miles) (US) 208",
    fld_fullspecifications: "Colors - Sage Green.\r\nGlacier Silver",
    fld_variants:
      "POWERTRAIN. 2 permanent-magnet synchronous AC motors. \r\nPower. 201 hp @ 13,900 rpm (each) \r\nTorque. 243 lb-ft @ 0 rpm (each) \r\nCombined Power. 402 hp. \r\nCombined Torque. 486 lb-ft. \r\nBattery Pack. liquid-cooled lithium-ion, 75.0 kWh.",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=TzxP3JdgYEc\r\nhttps://www.youtube.com/watch?v=u5_pgP5IZlk\r\nhttps://www.youtube.com/watch?v=y29veKTs1ds",
    fld_expertreview: "Not Announced",
    fld_gallery:
      "What is the expected price of Volvo XC40 Recharge?\r\nThe price of Volvo XC40 Recharge is expected to be Rs. 60.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Volvo XC40 Recharge?\r\nThe estimated launch date of Volvo XC40 Recharge is Jan 15, 2022.\r\n\r\nWhat are the expected key specifications of Volvo XC40 Recharge?\r\nVolvo XC40 Recharge will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Volvo XC40 Recharge?\r\nVolvo XC40 Recharge will be competed with X3, XF, 3 Series, 5 Series, S5 Sportback.",
  },
  {
    fld_name: "Maruti Futuro-e",
    fld_tagline: "Maruti's Leap Into the Future",
    fld_description:
      "At the 2020 Auto Expo, Indian car giant Maruti Suzuki India showcased its concept car named Futuro-e.\r\nThe car is an all-electric SUV coupe that draws attention for its futuristic design aesthetics.\r\nThe car is expected to use Artificial intelligence and change the way we drive forever.",
    fld_creative:
      "The Maruti Futuro-e has showcased a concept study SUV which demonstrates a bold new global design language along the lines of the coupe-SUV bodystyle. The Maruti Suzuki Futuro-e concept gets a coupe roofline which swoops at the rear, while it's a big and bold front that adds aggression to the car. It gets larger LED daytime running lights and wraparound LED taillights as well which basically gives it that modern and youthful appeal.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname: "Not Known",
    fld_specificationdescription: "Not Known",
    fld_keyfeatures: "Not Known",
    fld_fullspecifications: "Not Known",
    fld_variants: "Not Known",
    fld_specificitemsdesc: "https://youtu.be/oqwpn1XWRAI",
    fld_expertreview: 44593,
    fld_gallery:
      "What is the expected price of Maruti Futuro-e?\r\nThe price of Maruti Futuro-e is expected to be Rs. 15.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Maruti Futuro-e?\r\nThe estimated launch date of Maruti Futuro-e is Feb 10, 2022.\r\n\r\nWhat are the expected key specifications of Maruti Futuro-e?\r\nMaruti Futuro-e will be a suv available in Manual transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Maruti Futuro-e?\r\nMaruti Futuro-e will be competed with Hector, Safari, XUV700, Elantra, Astor.",
  },
  {
    fld_name: "Nissan Leaf",
    fld_tagline: "Is This Nissan's Best Car Ever? Check this out.",
    fld_description:
      "If advanced technology leaves fingerprints, Nissan LEAF is covered in them. \r\nThe Nissan Leaf is a fully electric car that is ready to give its competitors a tough fight for the EV supremacy. The EV comes loaded with modern features and efficient battery that give it a driving range of 360km.",
    fld_creative:
      "Roomy interior, optional semi-autonomous driving mode, perky acceleration from Plus models. Its starting price is attractive but the Leaf's driving range makes it a good choice only for drivers who don't plan to venture far from home. Standard range is uncompetitive, uses a less-common charging infrastructure, lacks the exciting performance of the leading EVs.\r\n\r\n",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Nissan Leaf\r\n\r\n40kwh\r\n\r\nNo\r\n\r\n98.5Mph\r\n\r\n8.0-inch infotainment system\r\n\r\nYes \r\n\r\n400km\r\n\r\nautomatic \r\n\r\nRs. 30 lakh*",
    fld_specificationdescription:
      "TYRE PRESSURE MONITORING SYSTEM & EASY-FILL TYRE ALERT. \r\nSix Airbags\r\nADVANCED CONTROL\r\nSMART BRAKING.\r\nANTI-LOCK BRAKING SYSTEM (ABS) \r\nTYRE PRESSURE MONITORING SYSTEM & EASY-FILL TYRE ALERT\r\nSix Airbags.",
    fld_keyfeatures:
      "VEHICLE TYPE\r\nfront-motor, front-wheel-drive, 5-passenger, 4-door hatchback\r\n\r\n\r\nMOTOR TYPE\r\npermanent-magnet synchronous AC motor, 215 hp, 251 lb-ft; 62.0-kWh lithium-ion battery pack \r\n\r\nTRANSMISSION\r\n1-speed direct-drive\r\n\r\nDIMENSIONS\r\nWheelbase: 106.3 in\r\nLength: 176.4 in\r\nWidth: 70.5 in\r\nHeight: 61.4 in\r\nPassenger volume: 93 cu ft\r\nCargo volume: 24 cu ft\r\nCurb weight: 3831 lb",
    fld_fullspecifications: "Colour - Grey",
    fld_variants:
      "Engine, Transmission, and Performance\r\nThe standard Leaf models come with a 147-horsepower electric motor that powers the front wheels; a 40.0-kWh battery pack provides the juice. Leaf Plus models come with a gutsier, 214-hp electric motor and a larger 62.0-kWh battery. The former managed a 7.4-second zero-to-60-mph time at our test track, but it feels perkier than this number suggests thanks to the instantaneous power delivery of the electric motor.\r\n\r\nRange, Charging, and Battery Life\r\nThe Leaf can be plugged in to a regular 120-volt outlet or a 240-volt outlet, but the charging times vary dramatically between the two. On a 240-volt connection, Nissan states that both the standard Leaf's battery and the larger one in the Leaf Plus can be replenished in seven hours.\r\n\r\nInterior, Comfort, and Cargo\r\nAlthough the cabin of the Leaf S and SVs has a lot of black plastic, the well-assembled and uniform textures help avoid it looking cheap. The SL model offers an optional light-gray leather interior with a matching dash pad that looks and feels better. The gauge cluster features a large analog speedometer next to a 7.0-inch digital readout that can be reconfigured to show a variety of displays.",
    fld_specificitemsdesc: "https://www.youtube.com/watch?v=CIxYY04HMx8\r\n",
    fld_expertreview: 44593,
    fld_gallery: "N/A",
  },
  {
    fld_name: "Tata Altroz EV",
    fld_tagline: "Will ALFA Platform Help Tata Sell More EV?",
    fld_description:
      "The homegrown carmaker unveiled electric variant of Altroz at Auto Expo 2020. This EV car is based on carmaker’s new Ziptron powertrain that can push the car to 500km driving mark. The EV will get a tweaked front end and a different set of alloy wheels.",
    fld_creative:
      "The Altroz ​​EV will be equipped with the same features as a regular Altroz. Floating touch screens, iRA connected car tech, ABS with EBD, and double airbags are available. Also, because this car is an electric car, it applies regenerative braking to save an extra mile of battery drain. \r\n",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Tata Altroz EV\r\n\r\nLi-ion\r\n\r\nNA\r\n\r\n100km/h\r\n\r\nNA\r\n\r\nNA\r\n\r\n250-300\r\n\r\nAutomatic \r\n\r\nRs. 12 Lakh* (Expected)",
    fld_specificationdescription:
      "It is available with the Automatic transmission. The Altroz EV car has a length of 3988 mm, width of 1754 mm and a wheelbase of 2501 mm.",
    fld_keyfeatures: "NA",
    fld_fullspecifications: "NA",
    fld_variants: "Not Known",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=3N-dA7NZTJU\r\nhttps://www.youtube.com/watch?v=NvklkZyYzEU\r\nhttps://www.youtube.com/watch?v=NvklkZyYzEU",
    fld_expertreview: 44621,
    fld_gallery:
      "What is the expected price of Tata Altroz EV?\r\nThe price of Tata Altroz EV is expected to be Rs. 14.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Tata Altroz EV?\r\nThe estimated launch date of Tata Altroz EV is Mar 13, 2022.\r\n\r\nWhat are the expected key specifications of Tata Altroz EV?\r\nTata Altroz EV will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Tata Altroz EV?\r\nTata Altroz EV will be competed with Venue, XUV300, Duster, Elantra, Astor.",
  },
  {
    fld_name: "Ford Mustang Mach E",
    fld_tagline: "Ford is Bold With Mach E",
    fld_description:
      "This EV car stole our hearts when it was introduced in 2019 Auto car show. \r\nThe car draws design inspiration from Mustang sports coupe that will be available in two fld_variants with 68kwh and 88kWh respectively. The expected range of the electric car is said to be 340 to 435km on a single full charge.",
    fld_creative:
      "The Mustang Mach-E is a terrific electric vehicle that also happens to be a terrific crossover. \r\n",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Ford Mustang Mach E\r\n\r\n68 kWh or 88 kWh usable capacity battery\r\n\r\nNA\r\n\r\n111mph\r\n\r\nAndroid based Display\r\n\r\nYEs\r\n\r\n483 km\r\n\r\nAutomatic \r\n\r\nRs. 70 lakh* (expected)",
    fld_specificationdescription:
      'SYNC® 4A with Enhanced Voice Recognition & Connected Built-in Navigation System\r\n\r\nPhone as a Key\r\n\r\n15.5" Touchscreen\r\n\r\n10.2" Digital Cluster screen\r\n\r\nFord Co-Pilot360TM 2.0 and Ford Co-Pilot360TM Assist 2.0',
    fld_keyfeatures:
      "VEHICLE TYPE\r\nfront- and mid-motor, all-wheel-drive, 5-passenger, 4-door wagon\r\n\r\n\r\nMOTORS\r\n2 permanent-magnet synchronous AC; combined output, 346 hp, 428 lb-ft; 88.0-kWh lithium-ion battery pack\r\n\r\nTRANSMISSIONS\r\n2 direct-drives\r\n\r\nCHASSIS\r\nBrakes (F/R): 14.3-in vented disc/12.4-in disc\r\nTires: Michelin Primacy A/S, 225/55R-19 103H M+S\r\n\r\nDIMENSIONS\r\nWheelbase: 117.5 in\r\nLength: 185.6 in\r\nWidth: 74.1 in\r\nHeight: 64.0 in",
    fld_fullspecifications: "SELECT\r\nCALIFORNIA ROUTE 1 \r\nPREMIUM\r\nGT",
    fld_variants:
      "Engine, Transmission, and Performance\r\nThe 2022 Mustang Mach-E is available with either a standard-range 70.0-kWh battery or an extended-range 91.0-kWh pack. These feed an electric motor mounted on the rear or both axles. \r\n\r\nRange, Charging, and Battery Life\r\nThe 2022 Mustang Mach-E has an EPA-estimated range of between 211 and 305 miles, depending on the battery pack and type of electric motors, which isn't nearly as impressive as the rangiest Tesla models.\r\n\r\nFuel Economy and Real-World MPG\r\nThe range-topping Mustang Mach-E with rear-wheel drive and the extended range battery boasts an EPA-estimated range of 305 miles.",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=Q8_LWJ6AXpY\r\nhttps://www.youtube.com/watch?v=jmTWpdC4-9I\r\nhttps://www.youtube.com/watch?v=MtSipP-Qedw",
    fld_expertreview: 44621,
    fld_gallery:
      "What is the expected price of Ford Mustang Mach E?\r\nThe price of Ford Mustang Mach E is expected to be Rs. 70.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Ford Mustang Mach E?\r\nThe estimated launch date of Ford Mustang Mach E is Mar 15, 2022.\r\n\r\nWhat are the expected key specifications of Ford Mustang Mach E?\r\nFord Mustang Mach E will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Ford Mustang Mach E?\r\nFord Mustang Mach E will be competed with X3, XF, 3 Series, 5 Series, S5 Sportback.\r\n",
  },
  {
    fld_name: "Renault Zoe",
    fld_tagline: "Can a Compact EV Ignite Indian Minds ?",
    fld_description:
      'Renault is all set to break the popular saying- "small cars are boring” with its Renault Zoe electric car.\r\nRenault has packed this EV with its 41 kW battery that pushes it to a range of 300 km in real mixed, urban or suburban use, as measured by the Worldwide Harmonised Light Vehicles Test Procedure.',
    fld_creative:
      "Regenerative braking: Every time the driver brakes, the\r\nbattery gets a boost of energy. This boost increases when\r\nyou switch New ZOE in to B Mode!\r\nHeat pump: The heat pump is able to air-condition and\r\nheat your car more efficiently than a traditional system,\r\nmeaning you can conserve more of your batteries energy\r\nfor driving range.\r\nChameleon Charger™: Individual to Renault, the\r\nChameleon Charger™ is always able to charge at the\r\nmaximum power(1) available and is therefore quicker and\r\nmore efficient, saving you time and money.\r\nPre-conditioning: Through Z.E. Services(2) you can pre-set\r\nthe temperature of the car so it’s ready for you before you\r\nget in. Warm in the winter and cool in the summer!\r\nECO Mode: When using the ECO Mode, the dynamic\r\nperformance and air conditioning or heating is limited to\r\nmaximize New ZOE’s driving range.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Renault Zoe /Hatchback \r\n\r\n54.66 \r\n\r\nYes\r\n\r\n140km/h\r\n\r\nYes\r\n\r\nYes\r\n\r\n395\r\n\r\nAutomatic \r\n\r\nRs. 8 Lakh*\r\n\r\n",
    fld_specificationdescription:
      "Fuel Type Electric \r\nMax Power (bhp@rpm) 179.69bhp \r\nMax Torque (nm@rpm) 245Nm \r\nSeating Capacity 5 \r\nTransmissionType Automatic",
    fld_keyfeatures:
      "Fuel Type:Electric(Battery)Range:NABattery Type:NACharging Time:NAEmission Norm Compliance:NATransmission:AutomaticBoot Space:NAPower Windows:NAAirbags:NAABS:NACentral Locking:NAFog Lamps:NA",
    fld_fullspecifications: "Not Announced",
    fld_variants: "Not Known",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=7dyMJwZQu6g\r\nhttps://www.youtube.com/watch?v=0TMutxumrUg\r\nhttps://www.youtube.com/watch?v=z6ZD1DN0_tk",
    fld_expertreview: 44621,
    fld_gallery:
      "What is the expected price of Renault Zoe?\r\nThe price of Renault Zoe is expected to be Rs. 8.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Renault Zoe?\r\nThe estimated launch date of Renault Zoe is Mar 20, 2022.\r\n\r\nWhat are the expected key specifications of Renault Zoe?\r\nRenault Zoe will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Renault Zoe?\r\nRenault Zoe will be competed with Santro, GO Plus, Grand i10 Nios, i20 N Line, Kicks.",
  },
  {
    fld_name: "Tesla Model Y",
    fld_tagline: "Will the US Heartthrob Enrapture Indian Market?",
    fld_description:
      "Unveiled in 2019, Tesla Model Y is electric compact crossover vehicle by Tesla Motors that is jacked-up SUV-esque version of the Model 3 hatchback.\r\nTesla Model Y has Tesla’s hallmark driving features like Tesla's adaptive cruise control system, forward collision warning, automatic emergency braking, pedestrian detection, blind-spot monitoring, rear cross-traffic alert.",
    fld_creative:
      "Tesla Model Y is more practical and more spacious than the smaller Model 3 sedan.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Tesla Y\r\n\r\n75 kWh lithium ion\r\n\r\nYes\r\n\r\n249 \r\n\r\nTouch \r\n\r\nYes \r\n\r\n326 miles \r\n\r\nAutopilot/Automatic \r\n\r\nRs. 70 Lakh* (Expected)",
    fld_specificationdescription:
      'Weight. 4,416 lbs. \r\nMax Cargo Volume. 76 cu ft. \r\nTop Speed. 155 mph. \r\nDisplays. 15" Center Touchscreen. \r\nSupercharging. Pay Per Use. \r\nWarranty. Basic Vehicle - 4 years or 50,000 mi, whichever comes first. Battery & Drive Unit - 8 years or 120,000 mi, whichever comes first',
    fld_keyfeatures:
      "VEHICLE TYPE\r\nfront- and mid-motor, all-wheel-drive, 5-passenger, 4-door wagon\r\n\r\n\r\nMOTORS\r\n1 permanent-magnet synchronous AC, 1 AC induction, 272 and 212 hp; combined output, 384 hp, 376 lb-ft; 80.5-kWh lithium-ion battery pack\r\n\r\nTRANSMISSION\r\n1-speed direct-drive\r\n\r\nCHASSIS\r\nSuspension (F/R): control arm/multilink\r\nBrakes (F/R): 14.0-in vented disc/13.2-in vented disc\r\nTires: Goodyear Eagle F1 Asymmetric 5, 255/40R-20 101W M+S TO\r\n\r\nDIMENSIONS\r\nWheelbase: 113.8 in\r\nLength: 187.0 in\r\nWidth: 75.6 in\r\nHeight: 63.9 in\r\nPassenger volume: 106 ft3\r\nCurb weight: 4386 lb",
    fld_fullspecifications: "NA",
    fld_variants:
      "Engine, Transmission, and Performance\r\nUnike the Model 3, the Model Y lineup no longer includes a Standard Range cost-leader trim. The Model Y's Long Range and Performance models are more compelling than a Standard Range trim anyway. The test resulted in the car running zero-to-60 mph in just 4.4 seconds, a mere 0.4 second behind the long-term Model 3 sedan.\r\n\r\nRange, Charging, and Battery Life\r\nThe all-wheel-drive Long Range offers 326 miles of range; the Performance model's estimated range drops to 303 miles. But the latter comes with 20-inch wheels, a lowered suspension, a higher top speed (145 mph), quicker claimed acceleration, and a dedicated Track mode. The cheapest Model Y is the Standard Range model but it comes with a lower driving range of 244 miles per charge.\r\n\r\nFuel Economy and Real-World MPG\r\nThe EPA estimates that the Long Range model is good for 131 MPGe in the city and 117 MPGe on the highway, while the Performance model's fuel economy estimates are 115 MPGe city and 106 MPGe highway.",
    fld_specificitemsdesc:
      "https://www.cnet.com/roadshow/reviews/2021-tesla-model-y-review/\r\n\r\nhttps://www.independent.co.uk/life-style/gadgets-and-tech/new-tesla-review-viral-autopilot-b1965125.html\r\n\r\nhttps://www.youtube.com/watch?v=XljN9iRlH8g\r\n\r\nhttps://www.youtube.com/watch?v=k3d8szO4a30\r\n\r\nhttps://www.cnet.com/roadshow/videos/2021-tesla-model-y-review-why-you-shouldnt-buy-teslas-littler-suv/",
    fld_expertreview: 44621,
    fld_gallery:
      "What is the estimated launch date of Tesla Model Y?\r\nThe estimated launch date of Tesla Model Y is Mar 29, 2022.\r\n\r\nWhat are the expected key specifications of Tesla Model Y?\r\nTesla Model Y will be a sedan available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Tesla Model Y?\r\nTesla Model Y will be competed with X3, XF, 3 Series, 5 Series, S5 Sportback.\r\n\r\nDoes the Tesla Model Y have a sunroof?\r\nTesla Model Y does not have a sunroof.",
  },
  {
    fld_name: "Renault K-ZE",
    fld_tagline: "Renault's Car That Charges Faster Than A Phone",
    fld_description:
      "Yes, You read it right! \r\nRenault unveiled the all-electric sibling of the Kwid at Auto Expo 2020 called the Renault K-ZE that charges faster than a Phone. Renault wants to bet on this EV that is based on the CMF-A platform that gets its power from 26.8kWh battery pack that can push it to a top speed of 105kmph.",
    fld_creative:
      'It is driven by a permanent magnet synchronous motor with a maximum speed of 105 km / h at 44 HP and 125 Nm. The electric motor is charged with a 26.8kWh battery with an NEDC range of 270km. There is also a "quick charge" option that can charge the battery from 0 to 80 percent in 30 minutes. In normal charging mode, it takes 4 hours for the battery to fully charge. The Kwid EV could be the wisest and most affordable electric car for the Indian masses. You just have to wait a long time to get here.',
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Renault K-ZE\r\n\r\n26.8kWh lithium-ion battery \r\n\r\nYes\r\n\r\n105kmph \r\n\r\nYes \r\n\r\nYEs\r\n\r\n271\r\n\r\nAutomatic \r\n\r\nRs. 6-10 Lakhs*",
    fld_specificationdescription:
      "Features: Renault has packed in an 8-inch touchscreen infotainment system, 4G WiFi connectivity, online music support, remote vehicle telemetry, a PM2. 5 air quality sensor and a tyre pressure monitoring system on the City K-ZE. \r\nFuel: Electric \r\nSeats: 5",
    fld_keyfeatures:
      "Fuel Type:Electric(Battery)Range:NABattery Type:NACharging Time:NAEmission Norm Compliance:NATransmission:AutomaticMax Power:NAMax Torque:125nmRear AC Ducts:NAAutomatic Climate Control:NAAudio Control:NACruise Control:NAFog Lamps:NAESP:NA",
    fld_fullspecifications: "NA",
    fld_variants:
      "Renault K-ZE is an ElectricCar. The electric motor generates a torque of 125nm. Renault K-ZE is available in Automatic transmission only. In configurations, Renault K-ZE has a dimensions of 3735 mm in length, 1579 mm in width and 1515 mm^3 mm in height.",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=eQ5ymZpVpt0\r\nhttps://www.youtube.com/watch?v=eQDb0zj87IU\r\nhttps://www.youtube.com/watch?v=7vKNdXf6xDM",
    fld_expertreview: 44896,
    fld_gallery:
      "What is the expected price of Renault K-ZE?\r\nThe price of Renault K-ZE is expected to be Rs. 10.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Renault K-ZE?\r\nThe estimated launch date of Renault K-ZE is Mar 31, 2022.\r\n\r\nWhat are the expected key specifications of Renault K-ZE?\r\nRenault K-ZE will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Renault K-ZE?\r\nRenault K-ZE will be competed with GO Plus, Figo, Grand i10 Nios, i20 N Line, Kicks.\r\n\r\nDoes the Renault K-ZE have a sunroof?\r\nRenault K-ZE does not have a sunroof.",
  },
  {
    fld_name: "Mahindra XUV300 Electric",
    fld_tagline: "Mahindra's Answer To Tata Nexon EV",
    fld_description:
      "Mahindra XUV300 electric SUV will be the brand’s first production model to be designed on (MESMA) architecture. The carmaker will use the same platform for its future EVs. \r\nThe electric XUV300 is likely to be offered in 2 fld_variants with 350V and 380V powertrains and two range 200km and 375km respectively.",
    fld_creative:
      "The Mahindra XUV300 Electric is very similar in overall design to the XUV300. However, the details of the XUV300 Electric are different. A closed grill on the front is expected.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Mahindra XUV300 Electric \r\n\r\n40kWh and the long-range 60kWh\r\n\r\nYes \r\n\r\n176kmh\r\n\r\nYes\r\n\r\nYes\r\n\r\n300Km \r\n\r\nAutomatic \r\n\r\nRs. 18 Lakh*",
    fld_specificationdescription: "Not Known",
    fld_keyfeatures:
      "Fuel Type:Electric(Battery)Range:NABattery Type:NACharging Time:NAEmission Norm Compliance:NATransmission:AutomaticMax Power:NAMax Torque:NARear AC Ducts:NAAutomatic Climate Control:NAAudio Control:NACruise Control:NAFog Lamps:NAESP:NA",
    fld_fullspecifications: "NA",
    fld_variants: "Not Known",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=nNYWZZxvHkA\r\nhttps://www.youtube.com/watch?v=2Ba-aSkhzrc\r\nhttps://www.youtube.com/watch?v=KvJKNYJ-1Ek",
    fld_expertreview: 44652,
    fld_gallery:
      "What is the expected price of Mahindra XUV300 Electric?\r\nThe price of Mahindra XUV300 Electric is expected to be Rs. 18.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Mahindra XUV300 Electric?\r\nThe estimated launch date of Mahindra XUV300 Electric is Apr 14, 2022.\r\n\r\nWhat are the expected key specifications of Mahindra XUV300 Electric?\r\nMahindra XUV300 Electric will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Mahindra XUV300 Electric?\r\nMahindra XUV300 Electric will be competed with Hector, Safari, XUV700, Alcazar, Superb.\r\n\r\nDoes the Mahindra XUV300 Electric have a sunroof?\r\nMahindra XUV300 Electric does not have a sunroof.",
  },
  {
    fld_name: "Ora R2",
    fld_tagline: "China's Most Affordable EV to Enter India Soon!",
    fld_description:
      "GWM’s electric division, ORA, has already unveiled its new EV R2 whose design is inspired from cats. \r\nThe EV claims a range of 401km and a mild performance improvement above its predecessor R1.",
    fld_creative:
      "Great Wall Motor brings a new budget electric vehicle, the ORAR2, to the global EV market. The OraR2 is a bit more expensive than the ORR1, but has more features and scope than the R1. ORAR1 covering a range of 300km, the same R2 covers a distance of 400km. The R2 continues to have a box-shaped design, with a wheelbase that is 15mm larger than the Ora R1.\r\n",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Ora r2\r\n\r\n34 kWh lithium polymer \r\n\r\nYes\r\n\r\n150km\r\n\r\nYes \r\n\r\nYes \r\n\r\n401km\r\n\r\nAutomatic Transmission\r\n\r\nRs. 10 Lakh*",
    fld_specificationdescription:
      "Ora R2 has a 23-inch touch screen system that comes with an instrument cluster and infotainment system. \r\nlow-cost positioning. \r\n6 airbags. \r\nwireless charging pad. \r\nelectronic stability program. \r\nlane change assist.",
    fld_keyfeatures:
      "Length: 4.235 mm. \r\nWidth: 1.825 mm. \r\nHeight: 1.596 mm. \r\nCurb weight: 1.510 kg (same weight for both batteries) \r\nGross weight: 1.885 kg. \r\nMaximum speed: 150 km/h. \r\nMotor: 105 kW and 210 N.m of torque. \r\nConsumption: 13 kWh/100 km (LFP battery) and 13,2 kWh/100 km (NCM battery)",
    fld_fullspecifications: "Standard - hatchback, Auto, Electric",
    fld_variants: "N/A",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=zt57J7yUEPw\r\nhttps://www.cardekho.com/ora-videos.htm\r\nhttps://www.youtube.com/watch?v=BcmXhIsj0Cs",
    fld_expertreview: 44682,
    fld_gallery:
      "What is the expected price of ORA R2?\r\nThe price of ORA R2 is expected to be Rs. 10.00 Lakh*.\r\n\r\nWhat is the estimated launch date of ORA R2?\r\nThe estimated launch date of ORA R2 is May 31, 2022.\r\n\r\nWhat are the expected key specifications of ORA R2?\r\nORA R2 will be a hatchback available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of ORA R2?\r\nORA R2 will be competed with GO Plus, Figo, Grand i10 Nios, i20 N Line, Kicks.\r\n\r\nDoes the ORA R2 have a sunroof?\r\nORA R2 does not have a sunroof.",
  },
  {
    fld_name: "Mercedes Benz EQS",
    fld_tagline: "Will Mercedes Be Able To Gain Dominance in Luxury EV?",
    fld_description:
      "If you are looking to buy a luxurious electric vehicle, look no further than Mercedes –EQ. \r\nThe EQS is the first all-electric luxury car from Mercedes that is based on modular architecture for luxury and executive-class electric vehicles. It seems like Mercedes EQ will be a head tuner when it rolls down on Indian roads.",
    fld_creative:
      "The EQS is the first fully electric luxury sedan from the Mercedes EQ. The EQS is also the first model based on a modular architecture for luxury and luxury car class electric vehicles. EQS combines technology, design, features and connectivity to inspire drivers and passengers alike.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "Mercedes-Benz EQS\r\n\r\n108kwh Li-ion battery\r\n\r\nyes \r\n\r\n180km/h\r\n\r\nTouch \r\n\r\nYes\r\n\r\n370 - 414 km\r\n\r\nAutomatic \r\n\r\nRs. 1.75cr*",
    fld_specificationdescription:
      'Along with the dazzling array of screens, the EQS features a fingerprint scanner that activates individual driver profiles, the "Hey Mercedes" voice-command system, and augmented-reality navigation in the head-up display. Every model also includes six USB-C ports throughout the cabin and a Burmester audio system.',
    fld_keyfeatures:
      "2022 Mercedes-Benz EQS\r\nVehicle Type: rear- or front- and rear-motor, rear- or all-wheel-drive, 4- or 5-passenger, 4-door sedan\r\n\r\nPRICE (C/D EST)\r\nBase: EQS450+, $110,000; EQS580 4Matic, $185,000\r\n\r\nPOWERTRAIN\r\nMotor(s): permanent-magnet synchronous AC, 329 or 516 hp, 419 or 631 lb-ft\r\nBattery Pack: liquid-cooled lithium-ion, 107.8 kWh\r\nOnboard Charger: 11.0 or 22.0 kW\r\nTransmission(s): direct-drive\r\n\r\nDIMENSIONS\r\nWheelbase: 126.4 in\r\nLength: 205.4 in\r\nWidth: 75.8 in\r\nHeight: 59.5 in\r\nTrunk Volume: 22 ft3\r\nCurb Weight (C/D est): 5600–5800 lb",
    fld_fullspecifications: "450+ and the 580 4Matic fld_variants",
    fld_variants:
      "Engine, Transmission, and Performance\r\nThe EQS450+ features an electric motor mounted on its rear axle that generates 329 horsepower and 406 pound-feet of torque. The EQS580 4Matic has an electric motor on each axle, which translates to all-wheel drive and a total system output of 516 horsepower and 611 pound-feet of torque\r\nRange, Charging, and Battery Life\r\nBoth EQS models feature a lithium-ion battery pack with a usable capacity of 107.8 kWh. The EPA rates the EQS450+ at 350 miles of range and the EQS580 at 340 miles. Along with the ability to charge the battery from 10 to 100 percent in approximately 11 hours and 15 minutes using a 240-volt (Level 2) charger, the EQS can supposedly replenish 70 percent of its battery–from 10 to 80 percent­–in about 35 minutes using a DC fast charger.",
    fld_specificitemsdesc:
      "https://www.youtube.com/watch?v=Mt8Y9uXBA08\r\nhttps://www.youtube.com/watch?v=_uI_hWmVCjo\r\n",
    fld_expertreview: 44531,
    fld_gallery:
      "What is the expected price of Mercedes-Benz EQS?\r\nThe price of Mercedes-Benz EQS is expected to be Rs. 1.75 Cr*.\r\n\r\nWhat is the estimated launch date of Mercedes-Benz EQS?\r\nThe estimated launch date of Mercedes-Benz EQS is Jun 01, 2022.\r\n\r\nWhat are the expected key specifications of Mercedes-Benz EQS?\r\nMercedes-Benz EQS will be a sedan available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Mercedes-Benz EQS?\r\nMercedes-Benz EQS will be competed with Phantom, Ghost, Rolls Royce Dawn, SF90 Stradale, Rolls Royce Wraith.\r\n\r\nDoes the Mercedes-Benz EQS have a sunroof?\r\nMercedes-Benz EQS does have a sunroof.",
  },
  {
    fld_name: "Hyundai Kona Electric 2022",
    fld_tagline: "Will The 2022 Hyundai Kona Regain India's Trust?",
    fld_description:
      "This electric car has a breathtaking design that enjoys a sporty electric acceleration and up to 484 km driving range on a single charge.\r\nWith an astonishing 8 yr warranty on its battery, this Hyundai EV has attracted millions of eyeballs.\r\n",
    fld_creative:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_generaldescription:
      "Kona Electric 2022\r\n\r\nLithium-ion Polymer/39.2 KWh\r\n\r\nYes\r\n\r\n165+ kmph\r\n\r\nAndroid Based Touch Screen \r\n\r\nYes\r\n\r\n483km \r\n\r\nAutomatic \r\n\r\nRs. 23.75 Lakhs*",
    fld_specificationname:
      "Electric powertrain — 415 km of driving range. The KONA Electric is seriously fun to drive and offers outstanding range of flexibility.\r\nConvenient charging – EV fast charger capability (100 kW) \r\nBattery temperature management system. \r\nHeat pump system.",
    fld_specificationdescription:
      "VEHICLE TYPE: front-motor, front-wheel-drive, 5-passenger, 4-door hatchback\r\n\r\nMOTOR TYPE: permanent-magnet synchronous AC, 201 hp, 290 lb-ft; 64.0-kWh lithium-ion battery pack\r\n\r\nTRANSMISSION: 1-speed direct drive\r\n\r\nCHASSIS\r\nSuspension (F/R): struts/multilink\r\nBrakes (F/R): 12.0-in vented disc/11.8-in disc\r\nTires: Nexen N'Priz AH8, 215/55R-17 94V M+S\r\n\r\nDIMENSIONS\r\nWheelbase: 102.4 in\r\nLength: 164.6 in\r\nWidth: 70.9 in\r\nHeight: 61.2 in\r\nPassenger volume: 93 cu ft",
    fld_keyfeatures: "NA",
    fld_fullspecifications:
      "Range, Charging, and Battery Life\r\nAggressive regenerative braking allows for true one-pedal driving and helps extend the driving range to a claimed 258 miles—one mile less than the Bolt EV. Pulling the paddles behind the steering wheel allows the driver to adjust the level of regen; we prefer the most aggressive setting.\r\n\r\nFuel Economy and Real-World MPG\r\nThe Kona Electric has earned a combined rating of 120 MPGe from the EPA. In our real-world highway fuel-economy testing, though, we recorded just 86 MPGe and 160 miles of driving range.\r\n\r\nInterior, Comfort, and Cargo\r\nThe Kona Electric's cabin is much the same as the one found inside its gas-powered counterpart: It's made from quality materials, is comfortable, and rich with features.",
    fld_variants:
      "https://www.youtube.com/watch?v=JcC8JjK_sJw\r\nhttps://www.youtube.com/watch?v=ZNu3UTTkvBE",
    fld_specificitemsdesc: 44774,
    fld_expertreview:
      "What is the expected price of Hyundai Kona Electric 2022?\r\nThe price of Hyundai Kona Electric 2022 is expected to be Rs. 23.75 Lakh*.\r\n\r\nWhat is the estimated launch date of Hyundai Kona Electric 2022?\r\nThe estimated launch date of Hyundai Kona Electric 2022 is Aug 15, 2022.\r\n\r\nWhat are the expected key specifications of Hyundai Kona Electric 2022?\r\nHyundai Kona Electric 2022 will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Hyundai Kona Electric 2022?\r\nHyundai Kona Electric 2022 will be competed with Alcazar, Hector Plus, Harrier, XUV700, Superb.\r\n\r\nDoes the Hyundai Kona Electric 2022 have a sunroof?\r\nHyundai Kona Electric 2022 does not have a sunroof.",
  },
  {
    fld_name: "MG ZS EV 2022",
    fld_tagline: "Will the MG Facelift Revive MG Electric brand ?",
    fld_description:
      "\r\nMG recently revealed the facelift of ZS EV that sports an enhanced driving range.\r\nThe electric car is powered by a new 51kWh battery for the base variant that can push the car to a staggering 317 kms of range. \r\nThe top variant of the car is loaded with a 72kWh battery that delivers a claimed range of 437kms.\r\n",
    fld_creative:
      "The MG ZS EV has been updated for global markets. The 2022 model brings in a raft of cosmetic changes as well as significant upgrades to the powertrain setup.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "ZS EV 2022\r\n\r\n72kWh Li-on battery\r\n\r\nYes\r\n\r\n140 kmph\r\n\r\nDigital instrument cluster and 10.1-inch touchscreen\r\n\r\nFloor mats, Spare wheels, Charging cables\r\n\r\n317 kms\r\n\r\nEco, Normal and Sport\r\n\r\nRs.22.00 Lakh*",
    fld_specificationdescription:
      "MG ZS EV 2022 is now powered by a new 51kWh battery for the base variant which is capable of delivering 317 kms range. The top variant of the car is loaded with a 72kWh battery which can deliver a claimed range of 437kms.",
    fld_keyfeatures:
      "VEHICLE TYPE\r\nfront-motor, front-wheel-drive, 5-passenger, 4-door hatchback\r\n\r\n\r\nMOTOR TYPE\r\nThree Phase Permanent Magnet Synchronous Motor, 140.8bhp@3500rpm, 350Nm@5000rpm, 72kWh Li-on battery\r\n\r\n\r\nTRANSMISSION\r\n1-speed direct-drive\r\n\r\nDIMENSIONS\r\nWheelbase: 2585 mm\r\nLength: 4314 mm\r\nWidth: 1809 mm\r\nHeight: 1620 mm\r\nCurb weight: 1,609 kg",
    fld_fullspecifications: "N/A",
    fld_variants:
      "Battery\r\nMG ZS EV’s 72kWh battery has the ability to get charged from 0-100 per cent in 10 hours 30 mins. It can only be achieved while using a 7kW AC charger. Users can also try and use a 100kW DC fast charger but will not be able to charge the battery in the above mentioned time frame.\r\n\r\nInterior Upgrades\r\nThese include a wireless phone charger, digital instrument cluster, a brand-new 10.1-inch touchscreen, iSmart connectivity suite, new graphics and improved functionality along with several new remote functions.\r\n\r\nExterior Upgrades\r\nIt has received a noticeable change in its design including slim headlamps with integrated LED Daytime Running Lights. The most prominent changes to the exterior include new front and rear bumpers paired with new alloy wheel designs.",
    fld_specificitemsdesc: "https://youtu.be/LHussYnVtxA",
    fld_expertreview: 44774,
    fld_gallery:
      "What is the expected price of MG ZS EV 2022?\r\nThe price of MG ZS EV 2022 is expected to be Rs. 22.00 Lakh.\r\n\r\nWhat is the launch date of MG ZS EV 2022?\r\nThe estimated launch date of MG ZS EV 2022 is August 2022.\r\n\r\nWhat are the key specifications off MG ZS EV 2022?\r\nMG ZS EV 2022 will be a SUV available in automatic transmission & electric options.\r\n\r\nWhat are the competitors of MG ZS EV 2022?\r\nMG ZS EV 2022 will be competed with Toyota Innova Crysta, Jeep Compass and Hyundai Tucson.\r\n\r\nWill MG ZS EV 2022 have a sunroof?\r\nNo, MG ZS EV 2022 will not have a sunroof.",
  },
  {
    fld_name: "Mercedes Benz EQA",
    fld_tagline: "Will Mercedes get everthing Right With EQA ?",
    fld_description:
      "This first fully electric compact SUV from Mercedes-Benz will change your perception of electric cars. \r\nWith unmatched comfort features and safety systems, the EQA provides an excellent electric driving experience.",
    fld_creative:
      "Mercedes-Benz EQA is a 5 seater suv. The Mercedes-Benz EQA is expected to launch in India in November 2022. The Mercedes-Benz EQA will rival X3, XF and 3 Series. Expect prices to start from Rs. 60.00 Lakh.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "EQA\r\n\r\nlithium-ion battery / 66.5 kWh.\r\n\r\nYes\r\n\r\n160 km/h\r\n\r\nDual-screen setup with up to 10.25-inch displays, blue accents for ambient lighting, and MBUX infotainment system with the digital assistant.\r\nYes\r\n\r\n426km\r\n\r\neco, comfort, sport\r\n\r\nRs 60 lakh*",
    fld_specificationdescription:
      "It is equipped with LED headlamps \r\nDual-screen setup with up to 10.25-inch displays \r\nBlue accents for ambient lighting, \r\nand MBUX infotainment system with the digital assistant.",
    fld_keyfeatures:
      "VEHICLE TYPE\r\nSUV\r\n\r\nPRICE AS TESTED\r\nRs 60 lakh*\r\n\r\nDIMENSIONS\r\nWheelbase: 2729 mm\r\nLength: 4463 mm\r\nWidth: 1834 mm\r\nHeight: 1620 mm\r\nCurb weight: 2470 kg",
    fld_fullspecifications:
      "EQA 300 4MATIC comes with 168 kW power\r\nEQA 350 4MATIC comes with 215 kW power",
    fld_variants:
      "Inside, the dash and furniture is basically as per a GLA. That means quality furniture, and two big graphically rich screens.\r\n\r\nIts 190bhp motor is hooked up to the front wheels. So it's officially called the EQA 250 because the performance, at least at sub-motorway speed, roughly matches a GLA 250 petrol.",
    fld_specificitemsdesc: "https://youtu.be/N2nqwVSFkn0",
    fld_expertreview: "Not announced",
    fld_gallery:
      "What is the expected price of Mercedes-Benz EQA?\r\nThe price of Mercedes-Benz EQA is expected to be Rs. 60.00 Lakh*.\r\n\r\nWhat is the estimated launch date of Mercedes-Benz EQA?\r\nThe estimated launch date of Mercedes-Benz EQA is Nov 04, 2022.\r\n\r\nWhat are the expected key specifications of Mercedes-Benz EQA?\r\nMercedes-Benz EQA will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of Mercedes-Benz EQA?\r\nMercedes-Benz EQA will be competed with X3, XF, 3 Series, 5 Series, S5 Sportback.\r\n\r\nDoes the Mercedes-Benz EQA have a sunroof?\r\nMercedes-Benz EQA does not have a sunroof.",
  },
  {
    fld_name: "Jaguar I-Pace",
    fld_tagline: "Will the Jaguar EV be the perfect gift for this Christmas?",
    fld_description:
      "The I-Pace from the JLR brand is undeniably a handsome beast that has a distinctive mesh grille, muscular build, and latest battery tech of 90 kWh lithium-ion that claims to give a range of above 300km. \r\nThe Jaguar E-Pace features just about every conceivable connectivity and infotainment gadget that you could wish for Christmas.",
    fld_creative:
      "The I-Pace might have started life as Jaguar’s first ever battery electric vehicle (BEV), but has since unwittingly become the face of a new Jaguar amidst the brand’s ambitious plan to go all-electric by 2025. Built on a bespoke EV platform and designed to make the most of the packaging freedom electric cars allow, the I-Pace is a Jaguar like no other in more ways than one.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname:
      "I-Pace\r\n\r\n90kWh lithium-ion battery pack \r\n\r\nYes\r\n\r\n200kmh\r\n\r\n31.24 cm (12.3) high-definition Interactive Driver Display \r\n\r\nNA\r\n\r\n470km\r\n\r\n1 speed Automatic\r\n\r\nRs 1.05 crore to Rs 1.12 crore",
    fld_specificationdescription:
      "It now offers a complimentary 4G data plan, a 360-degree camera system, and wireless charging. The optional Bright package adds chrome exterior trim, Atlas Grey accents, and other minor styling changes.",
    fld_keyfeatures:
      "Maximum power (kW) 294 \r\nMaximum torque (Nm) 696 \r\nTransmission Single Speed Automatic\r\nGross Vehicle Weight (GVW) (kg) 2 670\r\nHeight (mm) 1 566 \r\nLength (mm) 4 682\r\n",
    fld_fullspecifications: "I-Pace\r\nI-Pace Black",
    fld_variants:
      "LOW COST OF OWNERSHIP\r\nElectricity costs less than petrol or diesel. You may get some tax benefits.\r\n\r\n\r\nEASY AND CONVENIENT\r\nWith the majority of electric car charging done from home, you can more efficiently plan your time around the things you want to do. Simply recharge on an evening, and by morning you’ll be ready to go\r\n\r\nZERO TAILPIPE EMISSIONS\r\nVehicles powered solely by a battery are able to achieve maximum power with zero tailpipe emissions.\r\n\r\nA DISTINCTIVE DRIVE\r\nElectric technology is not only incredibly quiet, it’s able to deliver instant acceleration with no lag and no gearshift interruptions.",
    fld_specificitemsdesc: "https://youtu.be/oIZVgQ-Vogs",
    fld_expertreview: 44621,
    fld_gallery: 44278,
  },
  {
    fld_name: "BMW i3",
    fld_tagline: "BMW Blends High Tech EV With Ultra Luxury",
    fld_description:
      "This Bavarian electric car was unveiled at the Auto Expo 2018 that grabbed a lot of attention.\r\nThis luxurious electric car is packed with a 33KWh battery pack that produces 184PS of power and can propel the car from 0 to 100kmph in mere 6.9 seconds! If you are looking for inter- city commuting luxury then look no further than this wonder machine from BMW.\r\n",
    fld_creative:
      "Dynamic acceleration, spry in-town handling, suitably futuristic styling. Only short electric auto, dedicated space for rear passengers, no Android Auto. The i3 complements its electric powertrain with spatial styling and a modern interior, but it needs a longer range to be competitive.",
    fld_generaldescription:
      "Model \r\n\r\nBattery \r\n\r\nOn-Board Charger \r\n\r\nTop Speed \r\n\r\nDisplay \r\n\r\nUpgrades \r\n\r\nRange \r\n\r\nDrive Options \r\n\r\nEx-showroom Price",
    fld_specificationname: "N/A",
    fld_specificationdescription: "N/A",
    fld_keyfeatures: "N/A",
    fld_fullspecifications: "NA",
    fld_variants: "NA",
    fld_specificitemsdesc: "https://youtu.be/Q_mIuTxq-2o",
    fld_expertreview: "NA",
    fld_gallery:
      "What is the expected price of BMW i3?\r\nThe price of BMW i3 is expected to be Rs. 1.00 Cr*.\r\n\r\nWhat is the estimated launch date of BMW i3?\r\nThe estimated launch date of BMW i3 is Not Yet Announced.\r\n\r\nWhat are the expected key specifications of BMW i3?\r\nBMW i3 will be a suv available in Automatic transmission & Electric Range options.\r\n\r\nWhat are the alternatives/competitors of BMW i3?\r\nBMW i3 will be competed with Defender, X3 M, Q8, RS5, 7 Series.\r\n",
  },
];
