import Aaryaseason2 from "../Assets/webSeries/Aarya-season-2.jpg";
import Aranyak from "../Assets/webSeries/Aranyak.jpg";
import Asurseason2 from "../Assets/webSeries/Asur-season-2.jpg";
import Decoupled from "../Assets/webSeries/Decoupled.jpg";
import DelhiCrimeseason2 from "../Assets/webSeries/Delhi-Crime-season-2.jpg";
import EmilyinParisSeason2 from "../Assets/webSeries/Emily-in-Paris-Season-2.png";
import HushHush from "../Assets/webSeries/Hush-Hush.jpg";
import LostInSpaceSeason3 from "../Assets/webSeries/Lost-In-Space-Season-3.png";
import MasabaMasabaSeason2 from "../Assets/webSeries/Masaba-Masaba-Season-2.jpg";
import MismatchedSeason2 from "../Assets/webSeries/Mismatched-Season-2.jpg";
import Moneyheistseason5 from "../Assets/webSeries/Money-heist-season-5.jpg";
import QatilHaseenaonKeNaam from "../Assets/webSeries/Qatil-Haseenaon-Ke-Naam.jpg";
import Rudra from "../Assets/webSeries/Rudra.jpg";
import TheExpanseSeason6 from "../Assets/webSeries/The-Expanse-Season-6.png";
import TheWitcherSeason2 from "../Assets/webSeries/The-Witcher-Season-2.png";

export const webSeriesJson = [
  {
    fld_id: "1",
    fld_name: "Lost in Space 3",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "1 December,2021",
    fld_genre: "Action,Sci-fi",
    fld_language: "English",
    fld_seasonepisodes:
      "Seasons:3\r\nEpisodes:-\r\nSeason 1:10\r\nSeason 2:10\r\nSeason 3: 8",
    fld_actors:
      "Molly Parker as Maureen Robinson \n Toby Stephens as John Robinson\r\nMaxwell Jenkins as Will Robinson\r\nTaylor Russell as Judy Robinson\r\nMina Sundwall as Penny Robinson\r\nIgnacio Serricchio as Don West\r\nParker Posey as June Harris / Dr. Smith\r\nBrian Steele as the Robot",
    fld_producers: "Kevin Burns and Jon Janashi",
    fld_productionhouse:
      "Legendary Television, Synthesis Entertainment, Clickety-Clack Productions, and Applebox Entertainment",
    fld_plotsumprevseason:
      "Lost in Space, a reconceptualized version of the 1965 American science fiction series is based on the adventures of a family of space colonists whose spaceship veers off course.",
    fld_currentsynopsis:
      "In the third and final season of Lost in Space, The world of earth isn’t satisfying for the Robinson Family. The stakes are higher than ever and it is to be seen how the Robinson family live up the conditions in space. Get along with the Robinson family and see how their survival instincts will be put to the ultimate test. ",
    fld_prevrating: "7.5/10\r\n",
    fld_mediapartners:
      "The Teal Mango\r\nSpace.com\r\nDigital Spy\r\nOracle Globe\r\nThe Live Mirror\r\n",
    fld_funfacts:
      "The crew shot very little of Lost in Space on sets. The alien world the Robinsons are stranded on is an actual, dressed location instead of a bunch of sound stages papered with green screens.",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: LostInSpaceSeason3,
    fld_link: "https://www.youtube.com/embed/TiCxFQYpktU",
    fld_tittle: "How About Living In Space?",
    fld_view: "581",
    fld_like: "361",
    fld_share: "145",
  },
  {
    fld_id: "2",
    fld_name: "Money Heist season 5",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "3 December, 2021",
    fld_genre: "Action,Crime,Drama,Mystery,Thriller",
    fld_language: "English",
    fld_seasonepisodes:
      "Seasons:5(5th released in 2 parts)\r\nEpisodes:-\r\nPart 1: 13\r\nPart 2: 9\r\nPart 3: 8\r\nPart 4: 8\r\nPart 5:-(Further released in 2 parts)\r\nVolume 1: 5\r\nVolume 2: 5\r\n",
    fld_actors:
      "Enrique Arce as Arturo Roman\r\nMiguel Herran as Rio\r\nPedro Alonso as Berlin\r\nRodrigo de la Serna as Palermo\r\nItziar Ituno as Raquel Murillo\r\nJaime Lorente as Denver\r\nEsther Acebo as Monica Gaztambide\r\nBelen Cuesta as Manila\r\nDarko Peric as Helsinki\r\nNajwa Nimri as Alicia Sierra\r\nHovik Keuchkerian as Bogota\r\nLuka Peros as Marseille\r\nMario de la Rosa as Suarez\r\nFernando Cayo as Luis Tamayo\r\nFernando Soto as Angel Rubio\r\nJose Manuel Poga as Cesar Gandia",
    fld_producers: "Jesús Colmenar",
    fld_productionhouse: "Atresmedia, Pina-run Vancouver Media",
    fld_plotsumprevseason:
      "An unusual group of robbers attempt to carry out the most perfect robbery in Spanish history - stealing 2.4 billion euros from the Royal Mint of Spain.",
    fld_currentsynopsis:
      "The world of suspense has reached one notch above.Feed your curiosity and let all the questions be answered by Money Heist Part 5. This season will be nerve racking to watch. All the fans of Money Heist! Scratch the brand new episodes only on Netflix India.",
    fld_prevrating: "Imdb-8.3/10\r\nTimes Of India-3/5",
    fld_mediapartners:
      "Indulge Express\r\nNews 18\r\nYou Tube\r\nFacebook\r\nFox Exclusive\r\nKoi Moi\r\nPress Bolt News",
    fld_funfacts:
      '-Famous Brazilian football player, Neymar has a cameo appearance. He plays a monk.\r\n-The masks of the robbery are of the famous Spanish surrealist painter Salvador Dalí, who spoke highly of causing confusion and not limiting one\'s imagination - exactly what the Professor and the heist gang are doing.\r\n-The Bank of Spain did not allow the production crew to visit the real building to replicate it on set. The interior sets were mostly fictional. Acording to the team they used "a lot of gray and a dictatiorial flavour" to build the sets, inspired by the "Valley of the Fallen", a Francoist regime monument.\r\n-The outside of the building where the heist occurs isn\'t the actual Royal Mint of Spain but in fact the Spanish National Research Council in Madrid. There was an attempt to record it at the mint, which was denied by the institution. The interior scenes were recorded in a studio.',
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: Moneyheistseason5,
    fld_link: "https://www.youtube.com/embed/uwsmkWh0S5Y",
    fld_tittle: "Witness the robbery turn into war: The Final Call",
    fld_view: "831",
    fld_like: "511",
    fld_share: "245",
  },
  // {
  //   fld_id: "3",
  //   fld_name: "Qatil Haseenaon Ke Naam",
  //   fld_releaseplatform: "Zee5",
  //   fld_releasedate: "10 December,2021",
  //   fld_genre: "Crime, suspense, and thriller",
  //   fld_language: "Hindi",
  //   fld_seasonepisodes: "Season: 1\r\nEpisodes: 6",
  //   fld_actors:
  //     "Sanam Saeed, Sarwat Gilani, Samiya Mumtaz, Faiza Gillani, Beo Raana Zafar, Eman Suleman, Saleem Mairaj, Ahsan Khan, Sheheryar Munawar, and Osman Khalid Butt.",
  //   fld_producers: "Hasan Raza Abidi \r\nShailja Kejriwal",
  //   fld_productionhouse: "Zindagi",
  //   fld_plotsumprevseason: "NA",
  //   fld_currentsynopsis:
  //     "When men rule, they put forward their impulse but when women rule, they ensure justice! Get ready for the shades of sentiments, agony, angst, betrayal, love and revenge, a mix of emotions. Streaming now on Zee5.",
  //   fld_prevrating: "NA",
  //   fld_mediapartners:
  //     "Hindustan Times\r\nTimes of India\r\nOutlook India\r\nPinkvilla\r\nIndulge Express",
  //   fld_funfacts: "NA",
  //   fld_status: "Active",
  //   fld_updatedby: "0",
  //   fld_updatedon: "18/12/2021 10:30 AM",
  //   fld_image: QatilHaseenaonKeNaam,
  //   fld_link: "https://www.youtube.com/embed/azVZbkL0iUg",
  //   fld_tittle: "Shades Of Women",
  //   fld_view: "631",
  //   fld_like: "311",
  //   fld_share: "94",
  // },
  {
    fld_id: "4",
    fld_name: "Aarya Season 2",
    fld_releaseplatform: "Hotstar",
    fld_releasedate: "10 December,2021",
    fld_genre: "Crime ,Thriller, Drama",
    fld_language: "Hindi",
    fld_seasonepisodes: "Seasons:2\r\nEpisodes:-\r\nSeason 1: 9\r\nSeason 2: 8",
    fld_actors:
      "Sushmita Sen\r\nPratyaksh Panwar\r\nVikas Kumar\r\nJayant Kripalani\r\nSikander Kher\r\nVishwajeeth Pradhan\r\nAnkur Bhatia\r\nViren Vazirani\r\nVirti Vaghani",
    fld_producers: "Ram Madhvani Amita Madhvani Endemol Shine India",
    fld_productionhouse: "Endemol Shine India\r\nRam Madhvani Films",
    fld_plotsumprevseason:
      "The story of Aarya revolves around Aarya Sareen, who is a caring mother and a loving wife. She is quite naïve and is unaware of the illegal business of her husband. Things go badly for their family after Aarya's husband Tej Sareen, a pharma baron, is mysteriously killed. The drug mafia and illegal syndicates seek the death of his entire family. In response Aarya joins the gangs and becomes the mafia queen and seeks revenge against those who killed her husband.",
    fld_currentsynopsis:
      "Aarya stood up to every odd thrown at her, yet again she came up with strong determination and charged-up thoughts to execute her strength and turn the fight into battle. The brain-gripping story is ready to dictate your screen.",
    fld_prevrating: "Imdb-7.8/10",
    fld_mediapartners:
      "Blog To Bollywood\r\nThe Bulletin Time\r\nFi Ferst\r\nYou Tube\r\nThe MiracleTech\r\nThe Courier Daily",
    fld_funfacts:
      "-Aarya is a remake of the Dutch crime thriller series Penoza\r\n-For director Ram Madhvani, Aarya has been a work-in-progress for the longest time. He first envisioned his project as a feature film with Kajol in the lead, and after talks fell through, also had Madhuri Dixit Nene in the loop. The role finally landed with Sushmita Sen.\r\n",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: Aaryaseason2,
    fld_link: "https://www.youtube.com/embed/KSVD5w1zw0g",
    fld_tittle: "Aarya: Now The Warrior in Her",
    fld_view: "785",
    fld_like: "411",
    fld_share: "194",
  },
  // {
  //   fld_id: "5",
  //   fld_name: "Aranyak",
  //   fld_releaseplatform: "Netflix",
  //   fld_releasedate: "10 December,2021",
  //   fld_genre: "Mystery, Thriller, Drama",
  //   fld_language: "Hindi",
  //   fld_seasonepisodes: "Seasons:1\r\nEpisodes:7",
  //   fld_actors:
  //     "Zakir Hussain\r\nAshutosh Rana\r\nRaveena Tandon\r\nParambrata Chattopadhyay",
  //   fld_producers: "Rohan Sippy",
  //   fld_productionhouse: "Roy Kapur Films",
  //   fld_plotsumprevseason: "NA",
  //   fld_currentsynopsis:
  //     "The mysterious woods are grappling with so many unknown things. How will Ramika Sen(played by Raveena Tondon) solve this enigma? Let the envelope of curiosity unfold one by one. Raveena is all set to keep you intertwined with her sharp-edge acting and mesmerizing screen presence. \r\n",
  //   fld_prevrating: "NA",
  //   fld_mediapartners:
  //     "FreeKaaMaal\r\nLive Akhbar\r\nThe Envoy Web\r\nCinestaan",
  //   fld_funfacts:
  //     "-The program represents the way women juggle their careers, lives and emotional support that they receive or do not receive from home.The priorities for most women are their ‘children, in-laws, family’.This is what attracted Raveena to the character as everyone will bond with her, especially those in uniform.",
  //   fld_status: "Active",
  //   fld_updatedby: "0",
  //   fld_updatedon: "18/12/2021 10:30 AM",
  //   fld_image: Aranyak,
  //   fld_link: "https://www.youtube.com/embed/KcWcu1ytazY",
  //   fld_tittle: "Who’s in the Woods?",
  //   fld_view: "597",
  //   fld_like: "144",
  //   fld_share: "106",
  // },
  {
    fld_id: "6",
    fld_name: "The Expanse Season 6",
    fld_releaseplatform: "Amazon Prime",
    fld_releasedate: "10 December,2021",
    fld_genre: "Sci-fi, Thriller",
    fld_language: "English",
    fld_seasonepisodes:
      "Seasons:6\r\nEpisodes:-\r\nSeason 1:10\r\nSeason 2:13\r\nSeason 3:13\r\nSeason 4:10 \r\nSesaon 5:10\r\nSeason 6:6\r\n",
    fld_actors:
      "Steven Strait as James Holden, Dominique Tipper, Wes Chatham as Amos Burton, Shohreh Aghdashloo as Chrisjen Avasarala, and Frankie Adams as Bobbie Draper.",
    fld_producers:
      "Naren Shankar, Abraham and Franck. Andrew Kosove, Broderick Johnson, Laura Lancaster, Sharon Hall, Sean Daniel, Jason Brown, and Dan Nowak.",
    fld_productionhouse:
      "The Expanse is an epic space drama based on the best-selling novels of James S.A. Corey . Set in a future in which humanity has colonized the solar system, the show follows the people of Earth, Mars, and the Asteroid Belt who have long been in opposition with one another.\r\n",
    fld_plotsumprevseason:
      "The grippled life gets bitter with the emergence of an asteroid. Witness the wreck-havoc in the lives of these brave people. \r\n\r\n\r\n",
    fld_currentsynopsis: "9.5/10",
    fld_prevrating:
      "Collider.com\r\nSpace.com\r\nComicbook\r\nSlashgear\r\nBinged",
    fld_mediapartners:
      '-The Martian ship "Kittur Chennamma" appearing in episode 4 of season 3 is a reference to the Indian warrior queen Rani Chennamma from Kittur, Karnataka who fought against the British occupation of India.',
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: TheExpanseSeason6,
    fld_link: "https://www.youtube.com/embed/fb01ds-4IC4",
    fld_tittle: "What happens after the asteroid?",
    fld_view: "347",
    fld_like: "151",
    fld_share: "72",
  },
  // {
  //   fld_id: "7",
  //   fld_name: "Decoupled",
  //   fld_releaseplatform: "Netflix",
  //   fld_releasedate: "17 December ,2021",
  //   fld_genre: "Comedy, Drama,Romance",
  //   fld_language: "Hindi",
  //   fld_seasonepisodes: "Seasons:1\r\nEpisodes:NA",
  //   fld_actors:
  //     "R. Madhavan, Surveen Chawla,Sonia Rathee ,Shrestha Banerjee,Himanee Bhatia and Navneet Singh Kohli",
  //   fld_producers:
  //     "Vikramaditya Motwane Dipa De Motwane Bhavesh Mandalia Sejal Shah",
  //   fld_productionhouse: "Bombay Fables, Andolan Films",
  //   fld_plotsumprevseason: "NA",
  //   fld_currentsynopsis:
  //     "What happens when a misanthropic writer gets a disturbed married life and is just about to decouple with his wife. Witness the crispy edgy life of this couple and understand what it takes to be indifferent.",
  //   fld_prevrating: "NA",
  //   fld_mediapartners: "Imdb\r\nRepublic World\r\nIndian Express\r\nThe Hindu",
  //   fld_funfacts: "N/A",
  //   fld_status: "Active",
  //   fld_updatedby: "0",
  //   fld_updatedon: "18/12/2021 10:30 AM",
  //   fld_image: Decoupled,
  //   fld_link: "https://www.youtube.com/embed/QWd0F7UJ2F0",
  //   fld_tittle: "In The Life Of A Writer",
  //   fld_view: "471",
  //   fld_like: "248",
  //   fld_share: "105",
  // },
  {
    fld_id: "8",
    fld_name: "The Witcher Season 2",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "17 December, 2021",
    fld_genre: "Action, Adventure",
    fld_language: "English",
    fld_seasonepisodes: "Seasons:2\r\nEpisodes:-\r\nSeason 1:8\r\nSeason 2:8",
    fld_actors:
      "Returning from The Witcher season one are; Triss (Anna Shaffer), Cahir (Eamon Farren), Tissaia (MyAnna Buring), Yarpen Zigrin (Jeremy Crawford), Stregobor (Lars Mikkelson), Artorius (Terence Maynard), Sabrina (Therica Wilson Read), Filavandrel (Tom Canton), Murta (Lilly Cooper), and Istredd (Royce Pierreson).\r\n\r\nThe star new arrival is Kim Bodnia as Vesemir, while fellow new stars include Kristofer Hivju as Nivellen, Adjoa Andoh as Nenneke, Liz Carr as Fenn, Simon Callow as Codringher, Graham McTavish as Dijkstra, Kevin Doyle as Ba'Lian and Chris Fulton as Rience, among others.",
    fld_producers:
      "Sean Daniel \r\nJason Brown \r\nTomasz Bagiński \r\nJarosław Sawko \r\nLauren Schmidt Hissrich \r\nAlik Sakharov \r\nfld_producers \r\nMike Ostrowski",
    fld_productionhouse:
      "Sean Daniel Company \r\nStillking Films \r\nPlatige Image \r\nOne of Us \r\nCinesite",
    fld_plotsumprevseason:
      "The Witcher is an American fantasy drama based on the book series of the same name by Andrzej Sapkowski. The Witcher follows the story of Geralt of Rivia, a solitary monster hunter, who struggles to find his place in a world where people often prove more wicked than monsters and beasts. But when destiny hurtles him toward a powerful sorceress, and a young princess with a special gift, the three must learn to navigate independently the increasingly volatile Continent.",
    fld_currentsynopsis:
      "Season 1 was all about shaping characters and their introduction. Now, this time Geralt of Rivia and Princess Ciri are back to fight the fear and change their destiny. Watch out for this engrossing fantasy on 17 December on Netflix.",
    fld_prevrating: "8.2/10",
    fld_mediapartners: "Cinema Blend\r\nTech Radar\r\nCollider\r\nScreen Rant",
    fld_funfacts:
      "-Henry Cavill's role required extensive sword-training \r\n-He did not use any stunt doubles for any action scenes, Henry Cavill went through special gym training to prepare his body for the level of stunt work needed.",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: TheWitcherSeason2,
    fld_link: "https://www.youtube.com/embed/TJFVV2L8GKs",
    fld_tittle: "The Alfa Fantasy Saga-Blood of Elves",
    fld_view: "853",
    fld_like: "479",
    fld_share: "345",
  },
  {
    fld_id: "9",
    fld_name: "Emily in Paris Season 2",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "22 December, 2021",
    fld_genre: "Romantic, Comedy",
    fld_language: "English",
    fld_seasonepisodes: "Seasons:2\r\nEpisodes:-\r\nSeason 1:10\r\nSeason 2:NA",
    fld_actors:
      "Collins, Philippine Leroy-Beaulieu, Ashley Park, and Camille Razat in the lead roles, along with Lucas Bravo, Samuel Arnold, Bruno Gouery, and Kate Walsh.Netflix confirmed Jeremy O. Harris and Lucien Laviscount joined the cast of the drama-comedy series in significant roles.",
    fld_producers:
      "Stephen Joel Brown \r\nShihan Fey \r\nJake Fuller \r\nLily Collins \r\nRaphaël Benoliel",
    fld_productionhouse:
      "Darren Star Productions \r\nJax Media \r\nMTV Entertainment Studios",
    fld_plotsumprevseason:
      "Emily in Paris explores Emily Cooper’s experience with French culture and the Parisian life. Arriving in a posh but stiff-upper-lipped workplace, she is overwhelmed and struggles to navigate through the cultural differences but she makes some good friends and explores an exciting love life along the way.",
    fld_currentsynopsis:
      "After experiencing a wreck in her love life, Emily now wants her work life on track and how? How her work gets even more miserable despite a lot of efforts from her end. How will she balance her life?",
    fld_prevrating: 44387,
    fld_mediapartners: "Geo.Tv\r\nPinkvilla\r\nSlash Film\r\nCollider\r\nVogue",
    fld_funfacts: "N/A",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: EmilyinParisSeason2,
    fld_link: "https://www.youtube.com/embed/n22xzIyZr64",
    fld_tittle: "This Time Emily Is Determined",
    fld_view: "631",
    fld_like: "337",
    fld_share: "145",
  },
  // {
  //   fld_id: "10",
  //   fld_name: "Hush Hush",
  //   fld_releaseplatform: "Amazon Prime Video",
  //   fld_releasedate: "To be Announced",
  //   fld_genre: "Thriller, drama",
  //   fld_language: "Hindi",
  //   fld_seasonepisodes: "Seasons:1\r\nEpisodes:NA",
  //   fld_actors:
  //     "Juhi Chawla, Soha Ali Khan Pataudi, Sahana Goswami, Karishma Tanna, Kritika Kamra, and Ayesha Jhulka.",
  //   fld_producers: "Tanuja Chandra, Shikhaa Sharma",
  //   fld_productionhouse: "Abundantia Entertainment",
  //   fld_plotsumprevseason: "NA",
  //   fld_currentsynopsis:
  //     "If you like the individual stories of women, then Hush Hush is for you. The story discusses the life of different women who are balancing their lives through thick and thin. This era of realism has other realities of women to unleash. Get yourself ready for it.\r\n",
  //   fld_prevrating: "NA",
  //   fld_mediapartners:
  //     "The Envoy Web\r\nThe Hindu\r\nBollywood Bubble\r\nFirst Post\r\nCeleb Mantra",
  //   fld_funfacts: "N/A",
  //   fld_status: "Active",
  //   fld_updatedby: "0",
  //   fld_updatedon: "18/12/2021 10:30 AM",
  //   fld_image: HushHush,
  //   fld_link: "https://www.youtube.com/embed/QaJnBypH_0s",
  //   fld_tittle: "Women from All Walks of Life",
  //   fld_view: "391",
  //   fld_like: "176",
  //   fld_share: "85",
  // },
  {
    fld_id: "11",
    fld_name: "Masaba Masaba Season 2",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "To be Announced",
    fld_genre: "Biographical drama",
    fld_language: "English,Hindi both",
    fld_seasonepisodes: "Seasons:2\r\nEpisodes:-\r\nSeason 1: 6\r\nSeason 2: 6",
    fld_actors: "Masaba Gupta, Neena Gupta, Neil Bhoopalam, Rytasha Rathore",
    fld_producers: "Ashvini Yardi",
    fld_productionhouse: "Viniyard Films",
    fld_plotsumprevseason:
      "Masaba Masaba is based on the life of Masaba Gupta.It is a scripted version of the life of Masaba Gupta\r\nwhich includes Masaba’s family, her career, and her love life as a fashion designer. ",
    fld_currentsynopsis:
      "Peek into the life of Masaba and Neena Gupta again. Know Masaba as a Fashion Designer! Know Masaba as a girl-next-door! Know Masaba as a daughter! Know masaba as a person. Being a true fan of what Masaba does, this is going to be an exploring experience for you.",
    fld_prevrating: "6.7/10",
    fld_mediapartners:
      "The Indian Express\r\nMid Day\r\nIndia Today\r\nThe New India Express",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: MasabaMasabaSeason2,
    fld_link: "https://www.youtube.com/embed/DegkGdq9xy0",
    fld_tittle: "Life of Masaba: An Insight",
    fld_view: "674",
    fld_like: "361",
    fld_share: "168",
  },
  {
    fld_id: "12",
    fld_name: "Mismatched season 2",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "To be announced",
    fld_genre: "Romantic comedy",
    fld_language: "Hindi",
    fld_seasonepisodes: "Seasons:2\r\nEpisodes:-\r\nSeason 1: 6\r\nSeason 2: 6",
    fld_actors:
      "-Prajakta Koli as Dimple Ahuja\r\n-Rohit Saraf as Rishi Singh Shekhawat\r\n-Rannvijay Singha as Professor Siddharth Sinha (Sid)\r\n-Vidya Malavade as Zeenat Karim\r\n-Suhasini Mulay as Rishi's Grandmother\r\n-Nidhi Singh as Warden",
    fld_producers: "Ronnie Screwvala",
    fld_productionhouse:
      "SVF Entertainment Pvt. Ltd.\r\nRSVP\r\nPratibha studio",
    fld_plotsumprevseason:
      "The romantic series features two people who are not right for each other. The story is about Dimple's character who is a tech wizard and Rishi who found Dimple's profile on a matrimonial website, and is now interested in pursuing her.",
    fld_currentsynopsis:
      "A heartbroken Rishi is now left with a tornado of emotions after seeing Dimple and Harsh’s closeness. While Dimple encounters Namrata’s sexuality which is also a realization for her about society.A complete drama package for a perfect binge watch! ",
    fld_prevrating:
      "Imdb- 5.4/10\r\nTimes of India- 3/5\r\nNews18- 2.5/5\r\nCommon sense media- 3/5",
    fld_mediapartners:
      "Wikipedia\r\nImdb\r\nFilmcompanion\r\nBizAsia\r\nYou Tube\r\nThe envoy web\r\nBinged\r\n\r\n\r\n",
    fld_funfacts:
      "-Mismatched is based on Sandhya Menon's 2017 novel When Dimple Met Rishi. \r\n-The story was adapted to take place in Jaipur for the web series, where the entire shooting took place.The book originally is set in United States",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: MismatchedSeason2,
    fld_link: "https://www.youtube.com/embed/oorgtfS5MmQ",
    fld_tittle: "Will Rishi & Dimple Re-Unite?",
    fld_view: "784",
    fld_like: "441",
    fld_share: "275",
  },
  /*{
    fld_id: "13",
    fld_name: "Asur 2",
    fld_releaseplatform: "Voot",
    fld_releasedate: "To be announced",
    fld_genre: "Crime, Mystery, Thriller",
    fld_language: "Hindi",
    fld_seasonepisodes:
      "Seasons:2\r\nEpisodes:-\r\nSeason 1: 8\r\nSeason 2: NA",
    fld_actors:
      "Arshad Warsi as Dhananjay Rajput\r\nBarun Sobti as Nikhil Nair \r\nAnupriya Goenka as Naina Nair \r\nRidhi Dogra as Nusrat Saeed \r\nAmey Wagh as Rasool Shaikh \r\nPawan Chopra as Shashank Awasthi \r\nVishesh Bansal as Shubh",
    fld_producers: "Tanveer Bookwala",
    fld_productionhouse: "Ding Entertainment",
    fld_plotsumprevseason:
      "A psychopath killer with a twisted philosophy deep-seated in Indian mythology is on the loose. Two forensic officers from the CBI are close on heals to catch the serial killer but with a twist of fate, they get caught in a dreadful situation. How will this end? Will the real face of this ASUR be revealed?",
    fld_currentsynopsis:
      "Mix of bits of mythology, suspense, serial killing, Asur is the crime thriller you want to turn more and more. This time DJ Rajpoot is about to grab the serial killer(s). Get your popcorn to your table, turn the lights off and simulate the whole scenario of mouse and cat race-run. \r\n",
    fld_prevrating: "Imdb-8.4/10",
    fld_mediapartners: "Imdb\r\nFinance Rewind\r\nPinterest\r\nTelegraph Star",
    fld_funfacts: "N/A",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: Asurseason2,
  },
  {
    fld_id: "14",
    fld_name: "Delhi Crime season 2",
    fld_releaseplatform: "Netflix",
    fld_releasedate: "To be announced",
    fld_genre: "Crime, Thriller, Drama",
    fld_language: "Hindi",
    fld_seasonepisodes:
      "Seasons: 2\r\nEpisodes:-\r\nSeason1: 7\r\nSeason2: 7\r\n",
    fld_actors: "Shefali Shah, Rajesh Tailang, Rasika Dugal, Adil Hussain",
    fld_producers:
      "Florence Sloan, Aaron Kaplan, Jeff Sagansky, John Penotti, Kilian Kerwin, Mike Hogan, Pooja Kohli, Apoorva Bakshi, Sanjay Bachani",
    fld_productionhouse: "SK Global Entertainment, Golden Caravan",
    fld_plotsumprevseason:
      "Delhi Crime first season is based on the Nirbhaya Case, the brutal gang-rape incident that took place in Delhi in 2012. The series focuses on the police investigation that followed the horrific crime.It is an International Emmy-winning procedural drama.",
    fld_currentsynopsis:
      "The gripping crime thriller is all set with its season 2 to clutch your curiosity with its sharp-knitted storytelling. What happened that night on the road of Munirka? How will Vartika Chaturvedi reach the truth? Will she fall into the trap further or bring out the truth from it? Get the answer of all your knitted questions.\r\n",
    fld_prevrating: "8.5/10",
    fld_mediapartners:
      "The Bulletin Time\r\nOracle Globe\r\nIndia Today\r\nKoi moi\r\nBollywood Hungama\r\nImdb",
    fld_funfacts:
      "-Former Police Commissioner Neeraj Kumar, worked as a consultant for this web series.",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: DelhiCrimeseason2,
  },
  {
    fld_id: "15",
    fld_name: "Rudra – The Edge of Darkness",
    fld_releaseplatform: "Disney + Hotstar",
    fld_releasedate: "To be Announced",
    fld_genre: "crime-drama",
    fld_language: "Hindi",
    fld_seasonepisodes: "Seasons:1\r\nEpisodes:NA",
    fld_actors:
      "Ajay Devgan, Ileana D’cruz\r\nEsha Deol to make digital debut with Ajay Devgn",
    fld_producers: "Sameer Nair \r\nDeepak Segal",
    fld_productionhouse:
      "Applause Entertainment in association with BBC Studios India",
    fld_plotsumprevseason: "NA",
    fld_currentsynopsis:
      "Rudra- The Edge of Darkness is a new crime thriller that will keep you stuck to your place with its edgy storytelling. This time Ajay Devgan is all set to give you a different avatar of him as a cop. If you love to see twitchy suspense, get ready for this.",
    fld_prevrating: "NA",
    fld_mediapartners:
      "Indian Express\r\nImdb\r\nNDTV\r\nHindustan Times\r\nCinema Express",
    fld_funfacts:
      "The reason Ajay Devgn felt drawn to the series is because his character is intense, complex and dark.",
    fld_status: "Active",
    fld_updatedby: "0",
    fld_updatedon: "18/12/2021 10:30 AM",
    fld_image: Rudra,
  },*/
];
