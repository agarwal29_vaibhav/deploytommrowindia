import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import FooterPoliciesTab from "./Components/Footer/FooterPoliciesTab";
import Header from "./Components/Header";
import Gender from "./Components/Login/Gender";
import Login from "./Components/Login/Login";
import Otp from "./Components/Login/Otp";
import ProfileImg from "./Components/Login/ProfileImg";
import Signup from "./Components/Login/Signup";
import LandingPage from "./Components/MainView/LandingPage";
import MoviesPage from "./Components/MainView/MoviesPage";
import WebSeriesPage from "./Components/MainView/WebSeriesPage";
import ListingPage from "./Components/MainView/ListingPage";
import WebseriesListingPage from "./Components/MainView/WebseriesListingPage";

class Routing extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path={"/"} component={LandingPage} />
          <Route exact path={"/login"} component={Login} />
          <Route exact path={"/signUp"} component={Signup} />
          <Route exact path={"/otp-verification"} component={Otp} />
          <Route exact path={"/upload-image"} component={ProfileImg} />
          <Route exact path={"/gender"} component={Gender} />
          <Route exact path={"/policies"} component={LandingPage} />
          <Route exact path={"/all-movies-list"} component={ListingPage} />
          <Route exact path={"/movie"} component={MoviesPage} />
          <Route exact path={"/webseries"} component={WebSeriesPage} />
          <Route
            exact
            path={"/all-webseries-list"}
            component={WebseriesListingPage}
          />
        </div>
      </BrowserRouter>
    );
  }
}

export default Routing;
