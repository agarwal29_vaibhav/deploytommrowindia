import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Routes from "./Routes";

<style>
  @import
  url('https://fonts.googleapis.com/css2?family=Inter:wght@900&display=swap');
  @import
  url('https://fonts.googleapis.com/css2?family=Fira+Sans:wght@700&display=swap');
</style>;

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Routes></Routes>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
