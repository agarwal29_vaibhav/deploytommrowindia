import "./App.css";
import Footer from "./Components/Footer";
import LandingPage from "./Components/MainView/LandingPage";
import WebSeries from "./Components/MainView/WebSeriesPage";
import Header from "./Components/Header";
import FooterPoliciesTab from "./Components/Footer/FooterPoliciesTab";
import Login from "./Components/Login/Login";
import Signup from "./Components/Login/Signup";
import ContactUs from "./Components/StaticPages/ContactUs";
import Otp from "./Components/Login/Otp";
import ProfileImg from "./Components/Login/ProfileImg";
import Gender from "./Components/Login/Gender";

function App() {
  return (
    <div>
      <LandingPage />
      {/*<Header />
      <Footer />
      <Login />
      <Signup />      
      <Otp />
      <ProfileImg />
     <ContactUs />
      <Gender />*/}
    </div>
  );
}

export default App;
